# Задание №1. MPA, SPA, PWA

## MPA
Многостраничное приложение (MPA) это веб-сайт, состоящий из нескольких HTML-страниц, в основном отображаемых на сервере. Когда вы переходите на новую страницу, 
ваш браузер запрашивает новую страницу HTML с сервера. 
## SPA
это веб-приложение или веб-сайт, 
использующий единственный HTML-документ как оболочку для всех веб-страниц
## Преимущества SPA
преимуществам одностраничных приложений относятся следующие аспекты:

1. высокая скорость

2. хорошо отлаженная работа как на десктопных, так и на мобильных устройства

3. гибкость и отзывчивость за счёт отсутствия перезагрузки и повторного 
   рендеринга

4. оптимизированная и упрощённая разработка

## Недостатки SPA

1. увеличенная нагрузка на браузер

2. отсутствие JavaScript может стать проблемой для многих функций

3. необходимость обеспечить хорошую защиту данных

## PWA
Прогрессивное web-приложение (англ. progressive web app, PWA) — технология в web-разработке, 
которая визуально и функционально трансформирует сайт в приложение
## Преимущества PWA
1. простота в разработке
2. мгновенная установка

3. кроссплатформенность

4. минимальные требования к ресурсам устройств

5. быстрая загрузка онлайн и в автономном режиме благодаря кэшированию страниц

6. большая скорость по сравнению с нативными приложениям

## Недостатки PWA
1. Не всеми браузерами поддерживаются все функции;

2. сложность в поиске разработчиков: PWA – новая и пока ещё не слишком распространённая тенденция,
и поэтому далеко не все разработчики смогут с ней справиться.


## Различия 
В MPA, большая часть HTML-кода вашей страницы рендерится на сервере. 
В SPA, большая часть HTML отображается локально с помощью запуска JavaScript 
в браузере. 
Это оказывает значительное влияние на поведение сайта, производительность


PWA может работать без интернета и выводить push, почти конкуренция с мобильными приложениями

## Примеры SPA
https://www.GitHub.com, https://www.Facebook.com, https://www.airbnb.ru/

## Примеры MPA
https://www.dns-shop.ru/, https://www.Amazon.com, https://www.eBay.com

## Примеры PWA
https://web.whatsapp.com/
https://web.telegram.org/a/
https://ok.ru/

## Зачем PWA, Почему нельзя ограничиться SPA или MPA?
Внедрение PWA-приложения не отнимает много времени, 
не требует знаний в области программирования и реализуется бесплатно. 
Сегодня Google Play и App Store вводят все больше ограничений для 
разработчиков и пользователей, 
поэтому вместо создания мобильной версии сайта можно использовать PWA.

## Какие технологии используются для реализации PWA?
Обычно PWA разрабатываются с использованием веб-технологий HTML, CSS, 
JavaScript и JS-сред, таких как Angular или Vue
## Как можно понять (и можно ли), что перед нами PWA, а не другие виды приложений?
Расширение веб-приложения - .html, в то время как PWA - это браузерное приложение.
## Чем отличается между собой тестирование SPA и MPA?
SPA состоит из одной страницы и все взаимодействия происходят через 
запросы на обновление конкретных данных, 
то при ручном тестировании проверяется всё, что может обновляться.
в MPA надо тестировать все страницы.


## Как тестировать PWA?




