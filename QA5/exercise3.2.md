# Сайт https://online.sber.insure/store/propertyins/ 

Логотип //div[@ class="sber-logo"]


## Страница 1. "Выбор полиса"


### Что будет застраховано?

Хедер "Что будет застраховано?" //div/h4[last()-7] или //h4[1] или //*[text()='Что будет застраховано?']

Кнопка "Квартира" //*[text()='Что будет застраховано?']/following::button[1] 

Кнопка "Дом" //*[text()='Что будет застраховано?']/following::button[2] 


### Где находится недвижимость?

Поле "Регион проживания"  //*[text()='Где находится недвижимость?']/following::div[4]


### Страховая защита включенная в программу

Левая колонка под хедером "Страховая защита включенная в программу" //div[@class='sbi-form__row'] /div[@class='sbi-form__col-2'][1]/ul/. или //div[text()='Чрезвычайная ситуация']/ancestor::ul или //*[text()='Чрезвычайная ситуация']/ancestor::ul

Правая колонка под хедером "Страховая защита включенная в программу" //div[@class='sbi-form__row'] /div[@class='sbi-form__col-2'][2]/ul/. или //div[text()='Стихийные бедствия']/ancestor::ul или //*[text()='Стихийные бедствия']/ancestor::ul

Текстовый блок: "Падение летательных аппаратов и их частей"  //li[@class='calc-description__list-item']//div[@class='record__label'][text()='Падение летательных аппаратов и их частей'] или //*[text()='Падение летательных аппаратов и их частей'] 


### Срок действия страхования

Датапикер "Срок действия страхования" //*[text()='Срок действия страхования']/following::mat-datepicker-toggle


### Особенности объекта //*[text()='Особенности объекта']/following::button[1]

Первая строка левая кнопка //*[text()='Особенности объекта']/following::button[1]

Первая строка правая кнопка //*[text()='Особенности объекта']/following::button[2]

Вторая строка левая кнопка //*[text()='Особенности объекта']/following::button[3]

Вторая строка правая кнопка //*[text()='Особенности объекта']/following::button[4]

Третья строка левая кнопка //*[text()='Особенности объекта']/following::button[5]

Третья строка правая кнопка //*[text()='Особенности объекта']/following::button[6]


### Страховая сумма и объекты страхования

Слайдер суммы //div[@class="mat-slider-track-wrapper"]

Текстовый блок: "Мебель, техника и ваши вещи"  //*[text()=' Мебель, техника и ваши вещи '] или //div[@class='sbi-form__row ng-star-inserted']//div[@class='sbi-form__col-2'][text()=' Мебель, техника и ваши вещи '] 


### Промокод

Поле "Промокод" //*[text()='Промокод']/following::div[4] 

Кнопка "Применить промокод" //*[text()='Промокод']/following::button[1]

Кнопка "Оформить" //*[text()='Промокод']/following::button[2]

Кнопка "Принять" //*[text()='Промокод']/following::button[3]



## Страница 2 "Оформление"


### Страхователь

Кнопка "Заполнить по СберID" //*[text()='Страхователь']/following::button[1]

Поле "Фамилия" //*[text()='Страхователь']/following::div[contains(@class,'mat-form-field-flex')][1]

Поле "Имя" //*[text()='Страхователь']/following::div[contains(@class,'mat-form-field-flex')][2]

Поле "Отчество" //*[text()='Страхователь']/following::div[contains(@class,'mat-form-field-flex')][3]

Поле "Дата рождения" //*[text()='Страхователь']/following::div[contains(@class,'mat-form-field-flex')][4]

Чек-бокс "Отчество отсутствует" //*[text()='Страхователь']/following::mat-checkbox[1]

Датапикер "Дата рождения" //*[text()='Страхователь']/following::mat-datepicker-toggle[1]

Кнопка "Мужской" //*[text()='Страхователь']/following::button[3]

Кнопка "Женский" //*[text()='Страхователь']/following::button[4]


### Паспортные данные

Поле "Серия" //*[text()='Паспортные данные Страхователя']/following::div[contains(@class,'mat-form-field-flex')][1]

Поле "Номер" //*[text()='Паспортные данные Страхователя']/following::div[contains(@class,'mat-form-field-flex')][2]

Датапикер "Дата выдачи" //*[text()='Паспортные данные Страхователя']/following::mat-datepicker-toggle

Поле "Кем выдан" //*[text()='Паспортные данные Страхователя']/following::div[contains(@class,'mat-form-field-flex')][4]

Поле "Код подразделения" //*[text()='Паспортные данные Страхователя']/following::div[contains(@class,'mat-form-field-flex')][5]


### Адрес имущества

Поле "Регион" //*[text()='Адрес имущества']/following::div[contains(@class,'mat-form-field-flex')][1]

Поле "Город или населенный пункт" //*[text()='Адрес имущества']/following::div[contains(@class,'mat-form-field-flex')][2]

Поле "Улица" //*[text()='Адрес имущества']/following::div[contains(@class,'mat-form-field-flex')][3]

Поле "Дом" //*[text()='Адрес имущества']/following::div[contains(@class,'mat-form-field-flex')][4]

Поле "Квартира" //*[text()='Адрес имущества']/following::div[contains(@class,'mat-form-field-flex')][5]

Чек-бокс "Улица отсутсвует" //*[text()='Страхователь']/following::mat-checkbox[2]


### Контактные данные страхователя

Поле "Телефон" //*[text()='Контактные данные Страхователя']/following::div[contains(@class,'mat-form-field-flex')][1]

Поле "Электронная почта" //*[text()='Контактные данные Страхователя']/following::div[contains(@class,'mat-form-field-flex')][2]

Поле "Повтор электронной почты" //*[text()='Контактные данные Страхователя']/following::div[contains(@class,'mat-form-field-flex')][3]


Кнопка "Вернуться" //button[@class='mat-focus-indicator action-back mat-stroked-button mat-button-base']

Кнопка "Оформить" //button[@class='mat-focus-indicator mat-flat-button mat-button-base mat-accent']

Кнопка "Принять" //button[@class='cookie-button']




