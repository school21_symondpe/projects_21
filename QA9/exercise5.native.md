| | | | | | | | | | |
|-|-|-|-|-|-|-|-|-|-|
|Набор тестов|ID|Название|Результат теста|Ссылка на тест|Конфигурация|Автоматизирован|Дата/время результата|Автор|Дефекты|
|TUTU|131|Осмотр старта TUTU|Успешен|https://team-arpd.testit.software:443//projects/127/tests/131|Android 10 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|135|Запуск приложения на HD разрешении|Успешен|https://team-arpd.testit.software:443//projects/127/tests/135|Android 10 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|137|Введение данных профиля(+)|Успешен|https://team-arpd.testit.software:443//projects/127/tests/137|Android 10 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|138|Заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/138|Android 10 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|139|Пропал интернет Заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/139|Android 10 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|140|Осмотр старта TUTU в альбомном режиме|Успешен|https://team-arpd.testit.software:443//projects/127/tests/140|Android 10 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|141|Отмена оплаты билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/141|Android 10 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|142|Выбор прошлой даты билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/142|Android 10 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|143|Не даю согласие на обработку Заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/143|Android 10 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|144|Фильтр мест в вагоне Заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/144|Android 10 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|145|Пределы пассажиров Заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/145|Android 10 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|146|Дети до 2-х лет Заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/146|Android 10 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|147|Одинаковый город Заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/147|Android 10 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|148|Смена городов заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/148|Android 10 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|149|Заказ уведомления на билет ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/149|Android 10 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|150|Геолокация при выборе билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/150|Android 10 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|151|Сортировка вагонов Заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/151|Android 10 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|152|Выбор новой даты из раздела рейсов Заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/152|Android 10 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|153|Сеть 3G Заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/153|Android 10 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|154|Фильтр отправления вагонов Заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/154|Android 10 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|155|Фильтр Рейтинг вагонов Заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/155|Android 10 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|156|Фильтр стоимость вагонов Заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/156|Android 10 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|157|Фильтр время пути вагонов Заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/157|Android 10 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|158|Фильтр кнопкой Рейтинг 8+ вагонов Заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/158|Android 10 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|159|Фильтр кнопкой Отправление вечером вагонов Заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/159|Android 10 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|160|Выбор пасс. из Тип Вагона Заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/160|Android 10 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|161|Ограничение выбора мест Заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/161|Android 10 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|162|Уведомление предзаказа билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/162|Android 10 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|163|Смена языка приложения  TUTU|Успешен|https://team-arpd.testit.software:443//projects/127/tests/163|Android 10 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|164|Авторизация по паролю TUTU|Успешен|https://team-arpd.testit.software:443//projects/127/tests/164|Android 10 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|165|Лимит ввода при авторизаций по паролю TUTU|Успешен|https://team-arpd.testit.software:443//projects/127/tests/165|Android 10 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|166|Ввод спецсимволов в ОТКУДА ЖД|Успешен|https://team-arpd.testit.software:443//projects/127/tests/166|Android 10 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|167|Работа аппаратной кнопки назад|Успешен|https://team-arpd.testit.software:443//projects/127/tests/167|Android 10 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|168|Сеть 2G Заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/168|Android 10 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|180|Просмотр пассажиров TUTU|Успешен|https://team-arpd.testit.software:443//projects/127/tests/180|Android 10 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|181|Просмотр уведомлений TUTU|Успешен|https://team-arpd.testit.software:443//projects/127/tests/181|Android 10 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|131|Осмотр старта TUTU|Успешен|https://team-arpd.testit.software:443//projects/127/tests/131|Android 11 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|135|Запуск приложения на HD разрешении|Успешен|https://team-arpd.testit.software:443//projects/127/tests/135|Android 11 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|137|Введение данных профиля(+)|Успешен|https://team-arpd.testit.software:443//projects/127/tests/137|Android 11 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|138|Заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/138|Android 11 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|139|Пропал интернет Заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/139|Android 11 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|140|Осмотр старта TUTU в альбомном режиме|Успешен|https://team-arpd.testit.software:443//projects/127/tests/140|Android 11 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|141|Отмена оплаты билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/141|Android 11 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|142|Выбор прошлой даты билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/142|Android 11 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|143|Не даю согласие на обработку Заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/143|Android 11 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|144|Фильтр мест в вагоне Заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/144|Android 11 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|145|Пределы пассажиров Заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/145|Android 11 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|146|Дети до 2-х лет Заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/146|Android 11 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|147|Одинаковый город Заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/147|Android 11 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|148|Смена городов заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/148|Android 11 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|149|Заказ уведомления на билет ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/149|Android 11 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|150|Геолокация при выборе билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/150|Android 11 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|151|Сортировка вагонов Заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/151|Android 11 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|152|Выбор новой даты из раздела рейсов Заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/152|Android 11 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|153|Сеть 3G Заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/153|Android 11 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|154|Фильтр отправления вагонов Заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/154|Android 11 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|155|Фильтр Рейтинг вагонов Заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/155|Android 11 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|156|Фильтр стоимость вагонов Заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/156|Android 11 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|157|Фильтр время пути вагонов Заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/157|Android 11 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|158|Фильтр кнопкой Рейтинг 8+ вагонов Заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/158|Android 11 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|159|Фильтр кнопкой Отправление вечером вагонов Заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/159|Android 11 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|160|Выбор пасс. из Тип Вагона Заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/160|Android 11 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|161|Ограничение выбора мест Заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/161|Android 11 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|162|Уведомление предзаказа билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/162|Android 11 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|163|Смена языка приложения  TUTU|Успешен|https://team-arpd.testit.software:443//projects/127/tests/163|Android 11 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|164|Авторизация по паролю TUTU|Успешен|https://team-arpd.testit.software:443//projects/127/tests/164|Android 11 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|165|Лимит ввода при авторизаций по паролю TUTU|Успешен|https://team-arpd.testit.software:443//projects/127/tests/165|Android 11 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|166|Ввод спецсимволов в ОТКУДА ЖД|Успешен|https://team-arpd.testit.software:443//projects/127/tests/166|Android 11 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|167|Работа аппаратной кнопки назад|Успешен|https://team-arpd.testit.software:443//projects/127/tests/167|Android 11 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|168|Сеть 2G Заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/168|Android 11 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|180|Просмотр пассажиров TUTU|Успешен|https://team-arpd.testit.software:443//projects/127/tests/180|Android 11 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|181|Просмотр уведомлений TUTU|Успешен|https://team-arpd.testit.software:443//projects/127/tests/181|Android 11 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|131|Осмотр старта TUTU|Успешен|https://team-arpd.testit.software:443//projects/127/tests/131|Android 12 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|135|Запуск приложения на HD разрешении|Успешен|https://team-arpd.testit.software:443//projects/127/tests/135|Android 12 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|137|Введение данных профиля(+)|Успешен|https://team-arpd.testit.software:443//projects/127/tests/137|Android 12 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|138|Заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/138|Android 12 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|139|Пропал интернет Заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/139|Android 12 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|140|Осмотр старта TUTU в альбомном режиме|Успешен|https://team-arpd.testit.software:443//projects/127/tests/140|Android 12 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|141|Отмена оплаты билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/141|Android 12 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|142|Выбор прошлой даты билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/142|Android 12 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|143|Не даю согласие на обработку Заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/143|Android 12 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|144|Фильтр мест в вагоне Заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/144|Android 12 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|145|Пределы пассажиров Заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/145|Android 12 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|146|Дети до 2-х лет Заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/146|Android 12 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|147|Одинаковый город Заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/147|Android 12 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|148|Смена городов заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/148|Android 12 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|149|Заказ уведомления на билет ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/149|Android 12 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|150|Геолокация при выборе билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/150|Android 12 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|151|Сортировка вагонов Заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/151|Android 12 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|152|Выбор новой даты из раздела рейсов Заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/152|Android 12 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|153|Сеть 3G Заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/153|Android 12 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|154|Фильтр отправления вагонов Заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/154|Android 12 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|155|Фильтр Рейтинг вагонов Заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/155|Android 12 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|156|Фильтр стоимость вагонов Заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/156|Android 12 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|157|Фильтр время пути вагонов Заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/157|Android 12 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|158|Фильтр кнопкой Рейтинг 8+ вагонов Заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/158|Android 12 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|159|Фильтр кнопкой Отправление вечером вагонов Заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/159|Android 12 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|160|Выбор пасс. из Тип Вагона Заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/160|Android 12 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|161|Ограничение выбора мест Заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/161|Android 12 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|162|Уведомление предзаказа билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/162|Android 12 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|163|Смена языка приложения  TUTU|Успешен|https://team-arpd.testit.software:443//projects/127/tests/163|Android 12 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|164|Авторизация по паролю TUTU|Успешен|https://team-arpd.testit.software:443//projects/127/tests/164|Android 12 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|165|Лимит ввода при авторизаций по паролю TUTU|Успешен|https://team-arpd.testit.software:443//projects/127/tests/165|Android 12 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|166|Ввод спецсимволов в ОТКУДА ЖД|Успешен|https://team-arpd.testit.software:443//projects/127/tests/166|Android 12 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|167|Работа аппаратной кнопки назад|Успешен|https://team-arpd.testit.software:443//projects/127/tests/167|Android 12 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|168|Сеть 2G Заказ билета ЖД плацкарт|Успешен|https://team-arpd.testit.software:443//projects/127/tests/168|Android 12 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|180|Просмотр пассажиров TUTU|Успешен|https://team-arpd.testit.software:443//projects/127/tests/180|Android 12 Mobile Device|Нет|13.06.2023 17:58:35| | |
|TUTU|181|Просмотр уведомлений TUTU|Успешен|https://team-arpd.testit.software:443//projects/127/tests/181|Android 12 Mobile Device|Нет|13.06.2023 17:58:35| | |
