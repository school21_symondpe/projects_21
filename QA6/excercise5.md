## Swagger

###1.GET /api​/v1​/Activities
Метод: GET

Полный URL Запроса: https://fakerestapi.azurewebsites.net/api/v1/Activities

Заголовки запроса:  -H  "accept: text/plain; v=1.0"

Тело запроса (при наличии): нет

Статус-код ответа: 200

Тело ответа: 

[
  {
    "id": 1,
    "title": "Activity 1",
    "dueDate": "2023-06-08T04:56:28.2832889+00:00",
    "completed": false
  },
  {
    "id": 2,
    "title": "Activity 2",
    "dueDate": "2023-06-08T05:56:28.2832916+00:00",
    "completed": true
  },

###2.POST ​/api​/v1​/Activities

Метод: POST

Полный URL Запроса: https://fakerestapi.azurewebsites.net/api/v1/Activities

Заголовки запроса:  -H  "accept: text/plain; v=1.0" -H  "Content-Type: application/json; v=1.0"

Тело запроса (при наличии): нет

Статус-код ответа: 200

Тело ответа:
{
  "id": 0,
  "title": "string",
  "dueDate": "2023-06-08T02:53:44.383Z",
  "completed": true
}

###3.GET ​/api​/v1​/Activities​/{id}

Метод: GET

Полный URL Запроса: https://fakerestapi.azurewebsites.net/api/v1/Activities/1

Заголовки запроса:  -H  "accept: text/plain; v=1.0"

Тело запроса (при наличии): нет

Статус-код ответа: 200

Тело ответа:

{
  "id": 1,
  "title": "Activity 1",
  "dueDate": "2023-06-08T05:26:49.5370094+00:00",
  "completed": false
}

###4.PUT ​/api​/v1​/Activities​/{id}

Метод: PUT

Полный URL Запроса: https://fakerestapi.azurewebsites.net/api/v1/Activities/2

Заголовки запроса:  -H  "accept: text/plain; v=1.0" -H  "Content-Type: application/json; v=1.0"

Тело запроса (при наличии): {
  "id": 2,
  "title": "string",
  "dueDate": "2023-06-08T04:30:13.386Z",
  "completed": true
}

Статус-код ответа: 200

Тело ответа:

{
  "id": 2,
  "title": "string",
  "dueDate": "2023-06-08T04:30:13.386Z",
  "completed": true
}

###5.DELETE  ​/api​/v1​/Activities​/{id}

Метод: DELETE

Полный URL Запроса: https://fakerestapi.azurewebsites.net/api/v1/Activities/2

Заголовки запроса:  -H  "accept: */*"

Тело запроса (при наличии): no

Статус-код ответа: 200

Тело ответа: no

###6.GET ​/api​/v1​/Authors

Метод: GET

Полный URL Запроса: https://fakerestapi.azurewebsites.net/api/v1/Authors

Заголовки запроса:  -H  "accept: text/plain; v=1.0"

Тело запроса (при наличии): no

Статус-код ответа: 200

Тело ответа: 

[
  {
    "id": 1,
    "idBook": 1,
    "firstName": "First Name 1",
    "lastName": "Last Name 1"
  },
  {
    "id": 2,
    "idBook": 1,
    "firstName": "First Name 2",
    "lastName": "Last Name 2"
    
###7.POST ​/api​/v1​/Authors

Метод: POST

Полный URL Запроса: https://fakerestapi.azurewebsites.net/api/v1/Authors

Заголовки запроса:  -H  "accept: text/plain; v=1.0" -H  "Content-Type: application/json; v=1.0"

Тело запроса (при наличии): 

{
  "id": 0,
  "idBook": 0,
  "firstName": "string",
  "lastName": "string"
}


Статус-код ответа: 200

Тело ответа: 

{
  "id": 0,
  "idBook": 0,
  "firstName": "string",
  "lastName": "string"
}

###8.GET​/api​/v1​/Authors​/authors​/books​/{idBook}

Метод: GET

Полный URL Запроса: https://fakerestapi.azurewebsites.net/api/v1/Authors/authors/books/3

Заголовки запроса:  -H  "accept: text/plain; v=1.0"

Тело запроса (при наличии): NO


Статус-код ответа: 200

Тело ответа: 

[
  {
    "id": 7,
    "idBook": 3,
    "firstName": "First Name 7",
    "lastName": "Last Name 7"
  },
  {
    "id": 8,
    "idBook": 3,
    "firstName": "First Name 8",
    "lastName": "Last Name 8"
  },
  {
    "id": 9,
    "idBook": 3,
    "firstName": "First Name 9",
    "lastName": "Last Name 9"
  },
  {
    "id": 10,
    "idBook": 3,
    "firstName": "First Name 10",
    "lastName": "Last Name 10"
  }
]

###9.GET ​/api​/v1​/Authors​/{id}

Метод: GET

Полный URL Запроса: https://fakerestapi.azurewebsites.net/api/v1/Authors/6

Заголовки запроса:  -H  "accept: text/plain; v=1.0"

Тело запроса (при наличии): NO


Статус-код ответа: 200

Тело ответа: 

{
  "id": 6,
  "idBook": 2,
  "firstName": "First Name 6",
  "lastName": "Last Name 6"
}

###10.PUT ​/api​/v1​/Authors​/{id}

Метод: PUT

Полный URL Запроса: https://fakerestapi.azurewebsites.net/api/v1/Authors/7

Заголовки запроса:  -H  "accept: text/plain; v=1.0" -H  "Content-Type: application/json; v=1.0"

Тело запроса (при наличии): 

{
  "id": 0,
  "idBook": 0,
  "firstName": "string",
  "lastName": "string"
}



Статус-код ответа: 200

Тело ответа: 

{
  "id": 0,
  "idBook": 0,
  "firstName": "string",
  "lastName": "string"
}

###11.DELETE  ​/api​/v1​/Authors​/{id}

Метод: DELETE

Полный URL Запроса: https://fakerestapi.azurewebsites.net/api/v1/Authors/8

Заголовки запроса:  -H  "accept: */*"

Тело запроса (при наличии): NO

Статус-код ответа: 200

Тело ответа: NO

###12.GET ​/api​/v1​/Books

Метод: GET

Полный URL Запроса:https://fakerestapi.azurewebsites.net/api/v1/Books

Заголовки запроса:  -H  "accept: text/plain; v=1.0"

Тело запроса (при наличии): NO

Статус-код ответа: 200

Тело ответа: 

[
  {
    "id": 1,
    "title": "Book 1",
    "description": "Lorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\n",
    "pageCount": 100,
    "excerpt": "Lorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\n",
    "publishDate": "2023-06-07T05:07:13.9860654+00:00"
  },
  {
    "id": 2,
    "title": "Book 2",
    "description": "Lorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\n",
    "pageCount": 200,
    "excerpt": "Lorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\n",
    "publishDate": "2023-06-06T05:07:13.9860791+00:00"
  },
  
###13.POST ​/api​/v1​/Books

Метод: POST

Полный URL Запроса: https://fakerestapi.azurewebsites.net/api/v1/Books

Заголовки запроса:  -H  "accept: */*" -H  "Content-Type: application/json; v=1.0"

Тело запроса (при наличии): NO

Статус-код ответа: 200

Тело ответа: 

{
  "id": 0,
  "title": "string",
  "description": "string",
  "pageCount": 0,
  "excerpt": "string",
  "publishDate": "2023-06-08T05:08:09.455Z"
}

###14.GET ​/api​/v1​/Books​/{id}

Метод: GET

Полный URL Запроса: https://fakerestapi.azurewebsites.net/api/v1/Books/55

Заголовки запроса:  -H  "accept: text/plain; v=1.0"

Тело запроса (при наличии): NO

Статус-код ответа: 200

Тело ответа: 

{
  "id": 55,
  "title": "Book 55",
  "description": "Lorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\n",
  "pageCount": 5500,
  "excerpt": "Lorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\n",
  "publishDate": "2023-04-14T05:09:45.9614765+00:00"
}

###15.PUT ​/api​/v1​/Books​/{id}

Метод: PUT

Полный URL Запроса: https://fakerestapi.azurewebsites.net/api/v1/Books/66

Заголовки запроса:  -H  "accept: */*" -H  "Content-Type: application/json; v=1.0"

Тело запроса (при наличии): 

{
  "id": 0,
  "title": "string",
  "description": "string",
  "pageCount": 0,
  "excerpt": "string",
  "publishDate": "2023-06-08T05:11:02.710Z"
}



Статус-код ответа: 200

Тело ответа: 

{
  "id": 0,
  "title": "string",
  "description": "string",
  "pageCount": 0,
  "excerpt": "string",
  "publishDate": "2023-06-08T05:11:02.71Z"
}

###16.DELETE ​/api​/v1​/Books​/{id}

Метод: DELETE

Полный URL Запроса: https://fakerestapi.azurewebsites.net/api/v1/Books/77

Заголовки запроса:  -H  "accept: */*"

Тело запроса (при наличии): NO


Статус-код ответа: 200

Тело ответа: NO

###17.GET ​/api​/v1​/CoverPhotos

Метод: GET

Полный URL Запроса: https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos

Заголовки запроса:  -H  "accept: text/plain; v=1.0"

Тело запроса (при наличии): NO


Статус-код ответа: 200

Тело ответа: 

[
  {
    "id": 1,
    "idBook": 1,
    "url": "https://placeholdit.imgix.net/~text?txtsize=33&txt=Book 1&w=250&h=350"
  },
  {
    "id": 2,
    "idBook": 2,
    "url": "https://placeholdit.imgix.net/~text?txtsize=33&txt=Book 2&w=250&h=350"
  },
  
###18.POST ​/api​/v1​/CoverPhotos
 
 Метод: POST

Полный URL Запроса: https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos

Заголовки запроса:  -H  "accept: text/plain; v=1.0" -H  "Content-Type: application/json; v=1.0"

Тело запроса (при наличии): 

{
  "id": 0,
  "idBook": 0,
  "url": "string"
}

Статус-код ответа: 200

Тело ответа: 

{
  "id": 0,
  "idBook": 0,
  "url": "string"
}

###19.GET ​/api​/v1​/CoverPhotos​/books​/covers​/{idBook}

Метод: GET

Полный URL Запроса: https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/books/covers/23

Заголовки запроса:  -H  "accept: text/plain; v=1.0"

Тело запроса (при наличии): NO

Статус-код ответа: 200

Тело ответа: 

[
  {
    "id": 23,
    "idBook": 23,
    "url": "https://placeholdit.imgix.net/~text?txtsize=33&txt=Book 23&w=250&h=350"
  }
]

###20.GET ​/api​/v1​/CoverPhotos​/{id}

Метод: GET

Полный URL Запроса:  https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/11

Заголовки запроса:  -H  "accept: text/plain; v=1.0"

Тело запроса (при наличии): NO

Статус-код ответа: 200

Тело ответа: 

{
  "id": 11,
  "idBook": 11,
  "url": "https://placeholdit.imgix.net/~text?txtsize=33&txt=Book 11&w=250&h=350"
}

###21.PUT ​/api​/v1​/CoverPhotos​/{id}

Метод: PUT

Полный URL Запроса:  https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/12

Заголовки запроса: -H  "accept: text/plain; v=1.0" -H  "Content-Type: application/json; v=1.0"

Тело запроса (при наличии): 

{
  "id": 0,
  "idBook": 0,
  "url": "string"
}

Статус-код ответа: 200

Тело ответа: 

{
  "id": 0,
  "idBook": 0,
  "url": "string"
}

###22.DELETE ​/api​/v1​/CoverPhotos​/{id}

Метод: DELETE

Полный URL Запроса:  https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/71

Заголовки запроса: -H  "accept: */*"

Тело запроса (при наличии):  NO

Статус-код ответа: 200

Тело ответа: NO

###23.GET ​/api​/v1​/Users

Метод: GET

Полный URL Запроса:  https://fakerestapi.azurewebsites.net/api/v1/Users

Заголовки запроса: -H  "accept: text/plain; v=1.0"

Тело запроса (при наличии):  NO

Статус-код ответа: 200

Тело ответа: 

[
  {
    "id": 1,
    "userName": "User 1",
    "password": "Password1"
  },
  {
    "id": 2,
    "userName": "User 2",
    "password": "Password2"
  },
  
###24.POST ​/api​/v1​/Users

Метод: POST

Полный URL Запроса:  https://fakerestapi.azurewebsites.net/api/v1/Users

Заголовки запроса:-H  "accept: */*" -H  "Content-Type: application/json; v=1.0"

Тело запроса (при наличии):  

{
  "id": 0,
  "userName": "string",
  "password": "string"
}


Статус-код ответа: 200

Тело ответа: 

{
  "id": 0,
  "userName": "string",
  "password": "string"
}

###25.GET ​/api​/v1​/Users​/{id}

Метод: GET

Полный URL Запроса:  https://fakerestapi.azurewebsites.net/api/v1/Users/22

Заголовки запроса: -H  "accept: */*"

Тело запроса (при наличии):  NO

Статус-код ответа: 200

Тело ответа: 

{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.4",
  "title": "Not Found",
  "status": 404,
  "traceId": "00-396b0798d715c84ea25aafbd3c4b657d-7f3f55025f8de848-00"
}

###26.PUT ​/api​/v1​/Users​/{id}

Метод: PUT

Полный URL Запроса:  https://fakerestapi.azurewebsites.net/api/v1/Users/37

Заголовки запроса:  -H  "accept: */*" -H  "Content-Type: application/json; v=1.0"

Тело запроса (при наличии): 

{
  "id": 0,
  "userName": "string",
  "password": "string"
}

Статус-код ответа: 200

Тело ответа: 

{
  "id": 0,
  "userName": "string",
  "password": "string"
}

###27.DELETE ​/api​/v1​/Users​/{id}

Метод: PUT

Полный URL Запроса:  https://fakerestapi.azurewebsites.net/api/v1/Users/5

Заголовки запроса:  -H  "accept: */*"

Тело запроса (при наличии): NO

Статус-код ответа: 200

Тело ответа: NO

#Провальные

###1.POST ​/api​/v1​/Activities

Метод: POST

Полный URL Запроса:  https://fakerestapi.azurewebsites.net/api/v1/Activities

Заголовки запроса:  -H  "accept: text/plain; v=1.0" -H  "Content-Type: application/json; v=1.0"

Тело запроса (при наличии): 

{
  "id": 99999999999999,
  "title": "string",
  "dueDate": "2023-06-08T05:32:42.598Z",
  "completed": true
}



Статус-код ответа: 400

Тело ответа: 

{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-6d4604b265d51343a0c5bae8dbf3cdce-7e25c24650465047-00",
  "errors": {
    "$.id": [
      "The JSON value could not be converted to System.Int32. Path: $.id | LineNumber: 0 | BytePositionInLine: 20."
    ]
  }
}

###2.GET ​/api​/v1​/Activities​/{id}

Метод: GET

Полный URL Запроса:  https://fakerestapi.azurewebsites.net/api/v1/Activities/201

Заголовки запроса:  -H  "accept: text/plain; v=1.0"

Тело запроса (при наличии): NO


Статус-код ответа: 404

Тело ответа: 

{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.4",
  "title": "Not Found",
  "status": 404,
  "traceId": "00-c909ec22b7cf254d984e7c6bc1d74c22-fe0562adb8768842-00"
}

###3.PUT ​/api​/v1​/Activities​/{id}

Метод: PUT

Полный URL Запроса:  https://fakerestapi.azurewebsites.net/api/v1/Activities/2

Заголовки запроса:  -H  "accept: text/plain; v=1.0" -H  "Content-Type: application/json; v=1.0"

Тело запроса (при наличии): 

{
  "id": 25555555555555555555,
  "title": "string",
  "dueDate": "2023-06-08T04:30:13.386Z",
  "completed": true
}


Статус-код ответа: 400

Тело ответа: 

{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-aed2e0609f7b554491750c5a1a3d9902-524ddcf1f3edd64d-00",
  "errors": {
    "$.id": [
      "The JSON value could not be converted to System.Int32. Path: $.id | LineNumber: 0 | BytePositionInLine: 26."
    ]
  }
}

###4.POST ​/api​/v1​/Authors

Метод: POST

Полный URL Запроса:  https://fakerestapi.azurewebsites.net/api/v1/Authors

Заголовки запроса:  -H  "accept: text/plain; v=1.0" -H  "Content-Type: application/json; v=1.0"

Тело запроса (при наличии): 

{
  "id": 0,
  "idBook": 46264643525436,
  "firstName": "string",
  "lastName": "string"
}


Статус-код ответа: 400

Тело ответа: 

{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-5e072a1f3513f0489c5884ecab074751-df9d830652531045-00",
  "errors": {
    "$.idBook": [
      "The JSON value could not be converted to System.Int32. Path: $.idBook | LineNumber: 0 | BytePositionInLine: 31."
    ]
  }
}

###5.GET ​/api​/v1​/Authors​/authors​/books​/{idBook}

Метод: GET

Полный URL Запроса:  https://fakerestapi.azurewebsites.net/api/v1/Authors/authors/books/463642623423

Заголовки запроса:  -H  "accept: text/plain; v=1.0"

Тело запроса (при наличии): NO

Статус-код ответа: 400

Тело ответа: 

{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-c027b2606a716d4a8cb4c33aa23160e4-25b8c94025827740-00",
  "errors": {
    "idBook": [
      "The value '463642623423' is not valid."
    ]
  }
}

###6.GET ​/api​/v1​/Authors​/{id}

Метод: GET

Полный URL Запроса:  https://fakerestapi.azurewebsites.net/api/v1/Authors/8888888888888

Заголовки запроса:  -H  "accept: text/plain; v=1.0"

Тело запроса (при наличии): NO

Статус-код ответа: 400

Тело ответа: 

{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-dcb1b22e89a06946904d2c54bb201132-e4b74926ce1e324b-00",
  "errors": {
    "id": [
      "The value '8888888888888' is not valid."
    ]
  }
}

###7.PUT ​/api​/v1​/Authors​/{id}

Метод: PUT

Полный URL Запроса:  https://fakerestapi.azurewebsites.net/api/v1/Authors/44444444444444

Заголовки запроса:  -H  "accept: text/plain; v=1.0" -H  "Content-Type: application/json; v=1.0"

Тело запроса (при наличии):
{
  "id": 0,
  "idBook": 0,
  "firstName": "string",
  "lastName": "string"
}

Статус-код ответа: 400

Тело ответа: 

{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-b50f6d427c01c14ba7d0ba4af5cba1ca-1fec0d519ffd3b4f-00",
  "errors": {
    "id": [
      "The value '44444444444444' is not valid."
    ]
  }
}

###8.POST ​/api​/v1​/Books

Метод: POST

Полный URL Запроса:  https://fakerestapi.azurewebsites.net/api/v1/Books

Заголовки запроса:  -H  "accept: */*" -H  "Content-Type: application/json; v=1.0"

Тело запроса (при наличии):

{
  "id": 333333333333333,
  "title": "string",
  "description": "string",
  "pageCount": 0,
  "excerpt": "string",
  "publishDate": "2023-06-08T05:08:09.455Z"
}

Статус-код ответа: 400

Тело ответа: 

{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-eb26f5e69e87464f893acdaeabacbcce-8c29880fb4122345-00",
  "errors": {
    "$.id": [
      "The JSON value could not be converted to System.Int32. Path: $.id | LineNumber: 0 | BytePositionInLine: 21."
    ]
  }
}

###9.GET ​/api​/v1​/Books​/{id}

Метод: GET

Полный URL Запроса:  https://fakerestapi.azurewebsites.net/api/v1/Books/64624624643

Заголовки запроса:  -H  "accept: text/plain; v=1.0"

Тело запроса (при наличии): NO

Статус-код ответа: 400

Тело ответа: 

{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-0cdc4055b1623044a3ebbea5e377b286-50564419e4f16143-00",
  "errors": {
    "id": [
      "The value '64624624643' is not valid."
    ]
  }
}

###10.PUT ​/api​/v1​/Books​/{id}

Метод: PUT

Полный URL Запроса:  https://fakerestapi.azurewebsites.net/api/v1/Books/6677777777777777

Заголовки запроса:  -H  "accept: */*" -H  "Content-Type: application/json; v=1.0"

Тело запроса (при наличии):

{
  "id": 0,
  "title": "string",
  "description": "string",
  "pageCount": 0,
  "excerpt": "string",
  "publishDate": "2023-06-08T05:11:02.710Z"
}

Статус-код ответа: 400

Тело ответа: 

{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-acfdc174889c53458686d8e5926296a9-3abfd13ff2832a49-00",
  "errors": {
    "id": [
      "The value '6677777777777777' is not valid."
    ]
  }
}

###11.POST ​/api​/v1​/CoverPhotos

Метод: POST

Полный URL Запроса:  https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos

Заголовки запроса:  -H  "accept: text/plain; v=1.0" -H  "Content-Type: application/json; v=1.0"

Тело запроса (при наличии):

{
  "id": 222222222222222,
  "idBook": 0,
  "url": "string"
}

Статус-код ответа: 400

Тело ответа: 

{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-f37386d93f3b5847980ebeab54b1bd08-ce8fdebf0f51224e-00",
  "errors": {
    "$.id": [
      "The JSON value could not be converted to System.Int32. Path: $.id | LineNumber: 0 | BytePositionInLine: 21."
    ]
  }
}

###12.GET ​/api​/v1​/CoverPhotos​/books​/covers​/{idBook}

Метод: GET

Полный URL Запроса:  https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/books/covers/23563455735

Заголовки запроса:  -H  "accept: text/plain; v=1.0"

Тело запроса (при наличии): NO

Статус-код ответа: 400

Тело ответа: 
{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-f14ff61ab454504c961ba6438e5b5a06-59f3a4257d1ad24b-00",
  "errors": {
    "idBook": [
      "The value '23563455735' is not valid."
    ]
  }
}

###13.GET ​/api​/v1​/CoverPhotos​/{id}

Метод: GET

Полный URL Запроса:  https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/888888888888

Заголовки запроса:  -H  "accept: text/plain; v=1.0"

Тело запроса (при наличии): NO

Статус-код ответа: 400

Тело ответа: 

{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-88344617c222b7448e555671aaadfdd2-89681d191d842c49-00",
  "errors": {
    "id": [
      "The value '888888888888' is not valid."
    ]
  }
}

###14.PUT ​/api​/v1​/CoverPhotos​/{id}

Метод: PUT

Полный URL Запроса:  https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/222222333333333

Заголовки запроса:  -H  "accept: text/plain; v=1.0" -H  "Content-Type: application/json; v=1.0"

Тело запроса (при наличии): 

{
  "id": 0,
  "idBook": 0,
  "url": "string"
}

Статус-код ответа: 400

Тело ответа: 

{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-1e9f8921d09bb74c808064c2a960c147-98117d8bde42ce48-00",
  "errors": {
    "id": [
      "The value '222222333333333' is not valid."
    ]
  }
}

###15.POST ​/api​/v1​/Users

Метод: POST

Полный URL Запроса:  https://fakerestapi.azurewebsites.net/api/v1/Users

Заголовки запроса:  -H  "accept: */*" -H  "Content-Type: application/json; v=1.0"

Тело запроса (при наличии): 

{
  "id": 44444444444444,
  "userName": "string",
  "password": "string"
}

Статус-код ответа: 400

Тело ответа: 

{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-b13a78177fb1e949ac2f1e4b2d1f4b70-533596e4588b184a-00",
  "errors": {
    "$.id": [
      "The JSON value could not be converted to System.Int32. Path: $.id | LineNumber: 0 | BytePositionInLine: 20."
    ]
  }
}

###16.GET ​/api​/v1​/Users​/{id}

Метод: GET

Полный URL Запроса:  https://fakerestapi.azurewebsites.net/api/v1/Users/43555643342

Заголовки запроса:  -H  "accept: */*"

Тело запроса (при наличии): NO

Статус-код ответа: 400

Тело ответа: 

{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-68a9ea87d09f0a4c8df52aac33a8df4b-110acb96fadd3f45-00",
  "errors": {
    "id": [
      "The value '43555643342' is not valid."
    ]
  }
}

###17.PUT ​/api​/v1​/Users​/{id}

Метод: PUT

Полный URL Запроса:  https://fakerestapi.azurewebsites.net/api/v1/Users/4546246634

Заголовки запроса:  -H  "accept: */*" -H  "Content-Type: application/json; v=1.0"

Тело запроса (при наличии): 

{
  "id": 0,
  "userName": "string",
  "password": "string"
}

Статус-код ответа: 400

Тело ответа:

{
  "type": "https://tools.ietf.org/html/rfc7231#section-6.5.1",
  "title": "One or more validation errors occurred.",
  "status": 400,
  "traceId": "00-92e82e8841f55446a9b342a3d6e3bd9b-4e3ff4a8863d8a48-00",
  "errors": {
    "id": [
      "The value '4546246634' is not valid."
    ]
  }
}





















 









