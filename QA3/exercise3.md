## Тест-кейсы для проверки полей "Username" и "Password" на сайте https://www.saucedemo.com/ в дополнение с ролями: стандартного пользователя, проблемного пользователя, пользователя с проблемой производительности, заблокированного пользователя.

## Тест-кейс №1 Вход с ролью standard_user и корректный пароль
1. Предшествующие условия:
	- Находимся на сайте https://www.saucedemo.com/
2.  Шаги:
	- В поле USERNAME вводим логин с ролью standard_user
	- в поле PASSWORD вводим корректный пароль для роли
3.  Ожидаемый результат:
	- Выполнен вход на сайт.

## Тест-кейс №2 Вход с ролью standard_user и не корректный пароль
1. Предшествующие условия:
	- Находимся на сайте https://www.saucedemo.com/
2.  Шаги:
	- В поле USERNAME вводим логин с ролью standard_user
	- в поле PASSWORD вводим не корректный пароль для роли
3.  Ожидаемый результат:
	- Сообщение Epic sadface: Username and password do not match any user in this service.
	- вход на сайт не выполнен

## Тест-кейс №3 Вход с ролью standard_user и пустой пароль
1. Предшествующие условия:
	- Находимся на сайте https://www.saucedemo.com/
2.  Шаги:
	- В поле USERNAME вводим логин с ролью standard_user
	- в поле PASSWORD ничего не вводим
3.  Ожидаемый результат:
	- Сообщение Epic sadface: Password is required
	- вход на сайт не выполнен

## Тест-кейс №4 Вход с ролью problem_user и корректный пароль
1. Предшествующие условия:
	- Находимся на сайте https://www.saucedemo.com/
2.  Шаги:
	- В поле USERNAME вводим логин с ролью problem_user
	- в поле PASSWORD вводим корректный пароль для роли
3.  Ожидаемый результат:
	- Выполнен вход на сайт.

## Тест-кейс №5 Вход с ролью problem_user и не корректный пароль
1. Предшествующие условия:
	- Находимся на сайте https://www.saucedemo.com/
2.  Шаги:
	- В поле USERNAME вводим логин с ролью problem_user
	- в поле PASSWORD вводим не корректный пароль для роли
3.  Ожидаемый результат:
	- Сообщение Epic sadface: Username and password do not match any user in this service.
	- вход на сайт не выполнен

## Тест-кейс №6 Вход с ролью problem_user и пустой пароль
1. Предшествующие условия:
	- Находимся на сайте https://www.saucedemo.com/
2.  Шаги:
	- В поле USERNAME вводим логин с ролью problem_user
	- в поле PASSWORD ничего не вводим
3.  Ожидаемый результат:
	- Сообщение Epic sadface: Password is required
	- вход на сайт не выполнен

## Тест-кейс №7 Вход с ролью performance_glitch_user и корректный пароль
1. Предшествующие условия:
	- Находимся на сайте https://www.saucedemo.com/
2.  Шаги:
	- В поле USERNAME вводим логин с ролью performance_glitch_user
	- в поле PASSWORD вводим корректный пароль для роли
3.  Ожидаемый результат:
	- Выполнен вход на сайт.

## Тест-кейс №8 Вход с ролью performance_glitch_user и не корректный пароль
1. Предшествующие условия:
	- Находимся на сайте https://www.saucedemo.com/
2.  Шаги:
	- В поле USERNAME вводим логин с ролью performance_glitch_user
	- в поле PASSWORD вводим не корректный пароль для роли
3.  Ожидаемый результат:
	- Сообщение Epic sadface: Username and password do not match any user in this service.
	- вход на сайт не выполнен

## Тест-кейс №9 Вход с ролью performance_glitch_user и пустой пароль
1. Предшествующие условия:
	- Находимся на сайте https://www.saucedemo.com/
2.  Шаги:
	- В поле USERNAME вводим логин с ролью performance_glitch_user
	- в поле PASSWORD ничего не вводим
3.  Ожидаемый результат:
	- Сообщение Epic sadface: Password is required
	- вход на сайт не выполнен

## Тест-кейс №10 Вход с ролью locked_out_user и корректный пароль
1. Предшествующие условия:
	- Находимся на сайте https://www.saucedemo.com/
2.  Шаги:
	- В поле USERNAME вводим логин с ролью locked_out_user
	- в поле PASSWORD вводим корректный пароль для роли
3.  Ожидаемый результат:
	- Сообщение Epic sadface: Sorry, this user has been locked out.
	- вход на сайт не выполнен

## Тест-кейс №11 Вход с ролью locked_out_user и не корректный пароль
1. Предшествующие условия:
	- Находимся на сайте https://www.saucedemo.com/
2.  Шаги:
	- В поле USERNAME вводим логин с ролью locked_out_user
	- в поле PASSWORD вводим не корректный пароль для роли
3.  Ожидаемый результат:
	- Сообщение Epic sadface: Username and password do not match any user in this service.
	- вход на сайт не выполнен

## Тест-кейс №12 Вход с ролью locked_out_user и пустой пароль
1. Предшествующие условия:
	- Находимся на сайте https://www.saucedemo.com/
2.  Шаги:
	- В поле USERNAME вводим логин с ролью locked_out_user
	- в поле PASSWORD ничего не вводим
3.  Ожидаемый результат:
	- Сообщение Epic sadface: Password is required
	- вход на сайт не выполнен

## Тест-кейс №13 Вход с ролью не зарегистрированного пользователя и корректный пароль
1. Предшествующие условия:
	- Находимся на сайте https://www.saucedemo.com/
2.  Шаги:
	- В поле USERNAME вводим логин не зарегестрированный в системе
	- в поле PASSWORD вводим корректный пароль
3.  Ожидаемый результат:
	- Сообщение Epic sadface: Username and password do not match any user in this service.
	- вход на сайт не выполнен

## Тест-кейс №14 Вход с ролью не зарегистрированного пользователя и не корректный пароль
1. Предшествующие условия:
	- Находимся на сайте https://www.saucedemo.com/
2.  Шаги:
	- В поле USERNAME вводим логин не зарегестрированный в системе
	- в поле PASSWORD вводим не корректный пароль
3.  Ожидаемый результат:
	- Сообщение Epic sadface: Username and password do not match any user in this service.
	- вход на сайт не выполнен

## Тест-кейс №15 Вход с ролью не зарегистрированного пользователя и пустой пароль
1. Предшествующие условия:
	- Находимся на сайте https://www.saucedemo.com/
2.  Шаги:
	- В поле USERNAME вводим логин не зарегестрированный в системе
	- в поле PASSWORD ничего не вводим
3.  Ожидаемый результат:
	- Сообщение Epic sadface: Password is required.
	- вход на сайт не выполнен

## Тест-кейс №16 Вход без логина и корректный пароль для системы
1. Предшествующие условия:
	- Находимся на сайте https://www.saucedemo.com/
2.  Шаги:
	- В поле USERNAME ничего не вводим
	- в поле PASSWORD вводим корректный пароль для системы
3.  Ожидаемый результат:
	- Сообщение Epic sadface: Username is required
	- вход на сайт не выполнен

## Тест-кейс №17 Вход без логина и не корректный пароль для системы
1. Предшествующие условия:
	- Находимся на сайте https://www.saucedemo.com/
2.  Шаги:
	- В поле USERNAME ничего не вводим
	- в поле PASSWORD вводим не корректный пароль для системы
3.  Ожидаемый результат:
	- Сообщение Epic sadface: Username is required
	- вход на сайт не выполнен

## Тест-кейс №18 Вход без логина и пустой
1. Предшествующие условия:
	- Находимся на сайте https://www.saucedemo.com/
2.  Шаги:
	- В поле USERNAME ничего не вводим
	- в поле PASSWORD ничего не вводим
3.  Ожидаемый результат:
	- Сообщение Epic sadface: Username is required
	- вход на сайт не выполнен










