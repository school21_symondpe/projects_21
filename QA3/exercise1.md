## Тест-кейсы для проверки поля ввода возраста

## Тест-кейс №1 Корректные значения целые числа
1. Предшествующие условия:
	- Находимся в поле для ввода возраста
2.  Шаги:
	- В поле вводим число от 1 по 100
3.  Ожидаемый результат:
	- Уведомление пользователя, что было введено верное значение в поле ввода возраста.
## Тест-кейс №2 Некорретное значения меньше 1 года
1. Предшествующие условия:
	- Находимся в поле для ввода возраста
2.  Шаги:
	- В поле для ввода вводим число меньше 1
3.  Ожидаемый результат:
	- Уведомление пользователя, что было введено НЕверное значение в поле ввода возраста.
## Тест-кейс №3 Граничное значение 100 лет
1. Предшествующие условия:
	- Находимся в поле для ввода возраста
2.  Шаги:
	- В поле для ввода вводим число 100
3.  Ожидаемый результат:
	- Уведомление пользователя, что было введено верное значение в поле ввода возраста.
## Тест-кейс №4 Выход за граничное значение 100 лет
1. Предшествующие условия:
	- Находимся в поле для ввода возраста
2.  Шаги:
	- В поле для ввода вводим число 101
3.  Ожидаемый результат:
	- Уведомление пользователя, что было введено Неверное значение в поле ввода возраста.
## Тест-кейс №5 Граничное значение 1 год
1. Предшествующие условия:
	- Находимся в поле для ввода возраста
2.  Шаги:
	- В поле для ввода вводим цифру 1
3.  Ожидаемый результат:
	- Уведомление пользователя, что было введено верное значение в поле ввода возраста.
## Тест-кейс №6 Выход за граничное значение 1 год
1. Предшествующие условия:
	- Находимся в поле для ввода возраста
2.  Шаги:
	- В поле для ввода вводим цифру 0
3.  Ожидаемый результат:
	- Уведомление пользователя, что было введено НЕверное значение в поле ввода возраста.
## Тест-кейс №7 Числа больше 100 лет
1. Предшествующие условия:
	- Находимся в поле для ввода возраста
2.  Шаги:
	- В поле для ввода вводим число больше 101
3.  Ожидаемый результат:
	- Уведомление пользователя, что было введено НЕверное значение в поле ввода возраста.
## Тест-кейс №8 Не целые числа
1. Предшествующие условия:
	- Находимся в поле для ввода возраста
2.  Шаги:
	- В поле для ввода вводим дробное число
3.  Ожидаемый результат:
	- Уведомление пользователя, что было введено НЕверное значение в поле ввода возраста.
## Тест-кейс №9 Специальные символы
1. Предшествующие условия:
	- Находимся в поле для ввода возраста
2.  Шаги:
	- В поле для ввода вводим специальный символ
3.  Ожидаемый результат:
	- Уведомление пользователя, что было введено НЕверное значение в поле ввода возраста.
## Тест-кейс №10 Пустая строка
1. Предшествующие условия:
	- Находимся в поле для ввода возраста
2.  Шаги:
	- В поле для ввода ничего не вводим и нажимаем ВВОД
3.  Ожидаемый результат:
	- Уведомление пользователя, что было введено НЕверное значение в поле ввода возраста.
## Тест-кейс №11 Буквы
1. Предшествующие условия:
	- Находимся в поле для ввода возраста
2.  Шаги:
	- В поле для ввода вводим букву
3.  Ожидаемый результат:
	- Уведомление пользователя, что было введено НЕверное значение в поле ввода возраста.

	














