# Баг-репорты

## Баг №1

Заголовок: 
При обновлении сайта <https://sbermegamarket.ru/> блокируется сценарий

Предшествующие условия: 
Открыта страница <https://sbermegamarket.ru/>

Серьезность: 
Trivial

Шаги к воспроизведению:  
Обновить страницу <https://sbermegamarket.ru/>

Ожидаемый результат:
Ошибок нет

Фактический результат:  
Ошибка карты кода: Error: request failed with status 404
URL ресурса: https://mc.yandex.ru/metrika/tag.js
URL карты кода: tag.js.map
был заблокирован

Окружение:
Firefox 114.0.1 (64-разрядный)

## Баг №2

Заголовок: 
Ошибка во время загрузки ресурсов (502 Bad Gateway)

Предшествующие условия: 
Открыт сайт https://sbermegamarket.ru/

Серьезность: 
Minor

Шаги к воспроизведению:  
1. Открыть DevTools
2. Обновить страницу

Ожидаемый результат:
Ошибок нет

Фактический результат:  
GET
https://sbermegamarketru.webim.ru/button.php
[HTTP/1.1 502 Bad Gateway 325ms]

Окружение:
Firefox 114.0.1 (64-разрядный)

## Баг №3

Заголовок: 
не удалось выполнить запрос CORS

Предшествующие условия: 
Открыт сайт https://sbermegamarket.ru/

Серьезность: 
Minor

Шаги к воспроизведению:  
1. Открыть DevTools
2. Обновить страницу

Ожидаемый результат:
Ошибок нет

Фактический результат:  
Запрос из постороннего источника заблокирован: Политика одного источника запрещает чтение удаленного ресурса на https://cms-res.online.sberbank.ru/sberid/BlackList/Button/No_Button.json. (Причина: не удалось выполнить запрос CORS). Код состояния: (null).

Окружение:
Firefox 114.0.1 (64-разрядный)



## Баг №4

Заголовок: 
window.webim.api не определено

Предшествующие условия: 
Открыт сайт https://sbermegamarket.ru/

Серьезность: 
Minor

Шаги к воспроизведению:  
1. Открыть DevTools
2. Обновить страницу

Ожидаемый результат:
Ошибок нет

Фактический результат:  
Page url: https://sbermegamarket.ru/; message: watcher callback; details: TypeError, window.webim.api is undefined, resetWebimVisitor@https://extra-cdn.sbermegamarket.ru/static/dist/main.564c87f0.js:1:403345

Окружение:
Firefox 114.0.1 (64-разрядный)




## Баг №5

Заголовок: 
Запрос из постороннего источника заблокирован https://vk.com

Предшествующие условия: 
Открыт сайт https://sbermegamarket.ru/

Серьезность: 
Minor

Шаги к воспроизведению:  
1. Открыть DevTools
2. Обновить страницу
3. Нажать ссылку "О компании"

Ожидаемый результат:
Ошибок нет

Фактический результат:  
Запрос из постороннего источника заблокирован: Политика одного источника запрещает чтение удаленного ресурса на «https://vk.com/rtrg?p=VK-RTRG-1042270-gnJEy&products_event=view_other&price_list_id=343764&e=1&i=0&metatag_url=%2F%2Fsbermegamarket.ru%2Finfo%2Fabout-sbermegamarket-ru%2F&metatag_title=%D0%9E%20%D0%BA%D0%BE%D0%BC%D0%BF%D0%B0%D0%BD%D0%B8%D0%B8%20-%20%D0%9C%D0%B0%D1%80%D0%BA%D0%B5%D1%82%D0%BF%D0%BB%D0%B5%D0%B9%D1%81%20SberMegaMarket.ru». (Причина: Учётные данные не поддерживаются, если заголовок CORS «Access-Control-Allow-Origin» установлен в «*»).

Окружение:
Firefox 114.0.1 (64-разрядный)


