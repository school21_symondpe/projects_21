# Задание №6. Создание SQL-запросов

	
## Customers

1. Найти имена клиентов, проживающих в Германии

![Customers1.png](src/Customers1.png)

2. Найти кто проживает в городе Берн, Берлине или в стране США, группировать и сортировать по городу

![Customers2.png](src/Customers2.png)

3. Вывести кол-о проживающих в Германии и в городах содержащих в названии букву l, если таких больше 1 то показать количество

![Customers3.png](src/Customers3.png)


## Categories

1. Выбрать записи где в описании содержится 'ее' группировать и сортировать по категории

![Categories1.png](src/Categories1.png)

2. Подсчёт количества записей

![Categories2.png](src/Categories2.png)

3.Выбрать записи где в описании есть ff или an и сортировать по категории

![Categories3.png](src/Categories3.png)


## Employees

1. Выбрать записи с датой рождения от 01.01.1900 до 01.01.1960 содержащей в заметках слово ashingto

![Employees1.png](src/Employees1.png)

2. Выбрать записи с именем Nancy

![Employees2.png](src/Employees2.png)

3. Выбрать записи где в заметках есть слово toast

![Employees3.png](src/Employees3.png)


## OrderDetails

1. Количество записей с продуктом id 11

![OrderDetails1.png](src/OrderDetails1.png)

2. Подсчёт общего количества заказов по наименованиям продуктов

![OrderDetails2.png](src/OrderDetails2.png)

3. Подсчёт cуммы проданного в разбивке по наименованиям продуктов 

![OrderDetails3.png](src/OrderDetails3.png)


## Orders

1.Какой заказ, когда и какой сотрудник оформил

![Orders1.png](src/Orders1.png)

2.Записи с фамилией сотрудника, номером и датой заказа, сортировка по фамилии

![Orders2.png](src/Orders2.png)

3.Выборка по фамилии сотрудника и количеству заказов

![Orders3.png](src/Orders3.png)


## Products

1.Выбор продукта с названием'Chang'

![Products1.png](src/Products1.png)

2.Название продукта и наименование поставщика

![Products2.png](src/Products2.png)

3.Название продукта и категория

![Products3.png](src/Products3.png)


## Shippers

1.Подсчёт количества заказов по каждому грузоотправителю 

![Shippers1.png](src/Shippers1.png)

2.Показать номер заказа, дату заказа и название грузоотправителя

![Shippers2.png](src/Shippers2.png)

3.Выбор записей у которых в названии грузоотправителя есть 'Exp'

![Shippers3.png](src/Shippers3.png)


## Suppliers

1.Поставщики из Японии

![Suppliers1.png](src/Suppliers1.png)

2.Название поставщика, страна и размеры упаковки

![Suppliers2.png](src/Suppliers2.png)

3.Подсчёт поставщиков по странам

![Suppliers3.png](src/Suppliers3.png)






