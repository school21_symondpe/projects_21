## FakeRESTApi

### Activities

#### + GET ​/api​/v1​/Activities  
**URL**      
**Ожидаемый результат** Получение списка активностей      
**Заголовки запроса**   
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: f80b5e9f-acc3-4121-b79f-cabfd5557671
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
   
**Тело запроса**  отсутствует    

**Заголовки ответа**   
    Content-Type: application/json; charset=utf-8; v=1.0
Date: Sun, 11 Jun 2023 05:08:11 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0
 
**Тело ответа**
```
[{"id":1,"title":"Activity 1","dueDate":"2023-06-11T06:08:12.3507954+00:00","completed":false},{"id":2,"title":"Activity 2","dueDate":"2023-06-11T07:08:12.3507981+00:00","completed":true},{"id":3,"title":"Activity 3","dueDate":"2023-06-11T08:08:12.3507985+00:00","completed":false},


  ```

#### id bad -Activities: POST/Request body/44444444444  
**URL** https://fakerestapi.azurewebsites.net/api/v1/Activities     
**Ожидаемый результат** Внесение изм. с недоп.id отв.400
**Заголовки запроса**   
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 543e13be-4e01-4f4f-b8f9-25f256f7fe68
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
   
**Тело запроса**
```
{
  "id": 444444444444,
  "title": "string",
  "dueDate": "2023-06-10T10:57:45.455Z",
  "completed": true
}   

```


**Заголовки ответа**   
  Content-Type: application/problem+json; charset=utf-8
Date: Sun, 11 Jun 2023 05:13:09 GMT
Server: Kestrel
Transfer-Encoding: chunked
 
**Тело ответа**
```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-11e3fc3d61a01845a864bb5cab0f5b01-cff906a35e0d5545-00","errors":{"$.id":["The JSON value could not be converted to System.Int32. Path: $.id | LineNumber: 1 | BytePositionInLine: 20."]}}

```

#### +Activities: POST/Request body/5
**URL** https://fakerestapi.azurewebsites.net/api/v1/Activities     

**Ожидаемый результат** добавить запись 5

**Заголовки запроса**

Content-Type: application/json
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: eeb2af13-6960-4673-be9a-779e5b1a8b6a
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
{
  "id": 5,
  "title": "string",
  "dueDate": "2023-06-10T10:57:45.455Z",
  "completed": true
}

```

**Заголовки ответа** 

Content-Type: application/json; charset=utf-8; v=1.0
Date: Sun, 11 Jun 2023 05:20:29 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0

**Тело ответа** 

```
{"id":5,"title":"string","dueDate":"2023-06-10T10:57:45.455Z","completed":true}

```



#### id пусто -Activities: Activities: POST/Request body/{id пусто}
**URL** https://fakerestapi.azurewebsites.net/api/v1/Activities      

**Ожидаемый результат** добавить запись id пусто (ошибка) 

**Заголовки запроса**

Content-Type: application/json
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 233104f5-00bc-49d6-b634-8e75f6d8dd77
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
{
  "id": ,
  "title": "string",
  "dueDate": "2023-06-10T10:57:45.455Z",
  "completed": true
}

```

**Заголовки ответа** 

Content-Type: application/problem+json; charset=utf-8
Date: Sun, 11 Jun 2023 05:22:42 GMT
Server: Kestrel
Transfer-Encoding: chunked

**Тело ответа** 

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-b5780ea6e3514241b1f9ecc93dc68b50-c7088f669c556e4e-00","errors":{"$.id":["',' is an invalid start of a value. Path: $.id | LineNumber: 1 | BytePositionInLine: 8."]}}

```


#### id буквы -Activities: Activities: POST/Request body/{id QWERT}
**URL** https://fakerestapi.azurewebsites.net/api/v1/Activities     

**Ожидаемый результат** 
Добавление записи с id буквами, ошибка

**Заголовки запроса**
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: b04cf1bb-50be-4653-86d7-e3a9907cbc3b
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
{
  "id": QWER,
  "title": "string",
  "dueDate": "2023-06-10T10:57:45.455Z",
  "completed": true
}
```

**Заголовки ответа** 
Content-Type: application/problem+json; charset=utf-8
Date: Sun, 11 Jun 2023 05:24:54 GMT
Server: Kestrel
Transfer-Encoding: chunked

**Тело ответа** 

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-f2d29505585dfd4c9c3f0078f25ee1c3-02129d06d3d4774e-00","errors":{"$.id":["'Q' is an invalid start of a value. Path: $.id | LineNumber: 1 | BytePositionInLine: 8."]}}

```

#### Title=пусто - POST/Request body/
**URL**  https://fakerestapi.azurewebsites.net/api/v1/Activities    

**Ожидаемый результат** добавление записи title пусто (ошибка) 

**Заголовки запроса**
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 8a397f46-8ab6-4c5e-a143-2df7a2dcf8db
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```

{
    "id": 5,
    "title": ,
    "dueDate": "2023-06-10T10:57:45.455Z",
    "completed": true
}

```

**Заголовки ответа** 
Content-Type: application/problem+json; charset=utf-8
Date: Sun, 11 Jun 2023 05:26:24 GMT
Server: Kestrel
Transfer-Encoding: chunked

**Тело ответа** 

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-744377ccee6cf04ea9db89dd487eb031-e2ba593a7681004f-00","errors":{"$.title":["',' is an invalid start of a value. Path: $.title | LineNumber: 2 | BytePositionInLine: 13."]}}

```


#### Title=пусто - PUT ​/api​/v1​/Activities​/{id}
**URL** https://fakerestapi.azurewebsites.net/api/v1/Activities/{{activity_id}}     

**Ожидаемый результат** изменения в записи с title пусто (ошибка)

**Заголовки запроса**
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 48857bf8-e127-44a0-adda-ba98aecbfc01
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
{
    "id": 5,
    "title": ,
    "dueDate": "2023-06-10T10:57:45.455Z",
    "completed": true
}

```

**Заголовки ответа** 
Content-Type: application/problem+json; charset=utf-8
Date: Sun, 11 Jun 2023 05:29:04 GMT
Server: Kestrel
Transfer-Encoding: chunked

**Тело ответа** 

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-aa2d62698476c84ab7f70da86e34f568-d9398278b535ec48-00","errors":{"$.title":["',' is an invalid start of a value. Path: $.title | LineNumber: 2 | BytePositionInLine: 13."]}}

```

#### id=5 + GET ​/api​/v1​/Activities
**URL** https://fakerestapi.azurewebsites.net/api/v1/Activities/{{activity_id}}     

**Ожидаемый результат** получение данных по 5 id

**Заголовки запроса**
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 399d3bf4-b0e9-4c00-b346-86ef0d512e26
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
no
```

**Заголовки ответа** 
Content-Type: application/json; charset=utf-8; v=1.0
Date: Sun, 11 Jun 2023 05:30:35 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0

**Тело ответа** 

```
{"id":5,"title":"Activity 5","dueDate":"2023-06-11T10:30:36.0887999+00:00","completed":false}

```

#### id=LONG - GET ​/api​/v1​/Activities
**URL** https://fakerestapi.azurewebsites.net/api/v1/Activities{{long_id}}     

**Ожидаемый результат** получение данных по длинному id (ошибка)

**Заголовки запроса**
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: f54a68ed-8ffe-4cce-8584-e614a660130e
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
**Тело запроса**

```
no
```

**Заголовки ответа** 
Content-Length: 0
Date: Sun, 11 Jun 2023 05:32:07 GMT
Server: Kestrel

**Тело ответа** 

```
no
```

#### id=пусто - GET ​/api​/v1​/Activities
**URL** https://fakerestapi.azurewebsites.net/api/v1/Activities/{{empty_id}}     

**Ожидаемый результат** получение данных без id

**Заголовки запроса**
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 2b5dcada-be78-4362-a8d0-4a80dcae1a98
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
**Тело запроса**


```
0
```

**Заголовки ответа** 
Content-Type: application/json; charset=utf-8; v=1.0
Date: Sun, 11 Jun 2023 05:33:40 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0

**Тело ответа** 

```
[{"id":1,"title":"Activity 1","dueDate":"2023-06-11T06:33:40.417812+00:00","completed":false},{"id":2,"title":"Activity 2","dueDate":"2023-06-11T07:33:40.4178143+00:00","completed":true},{"id":

```

#### id=буквы - GET ​/api​/v1​/Activities
**URL** https://fakerestapi.azurewebsites.net/api/v1/Activities/{{words_id}}     

**Ожидаемый результат** получение данных с id буквами (ошибка)

**Заголовки запроса**
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: f4f9ac2c-68de-4aa6-a001-ef98980a1537
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
0
```

**Заголовки ответа** 
Content-Type: application/problem+json; charset=utf-8
Date: Sun, 11 Jun 2023 05:35:00 GMT
Server: Kestrel
Transfer-Encoding: chunked

**Тело ответа** 

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-0e69270e9c5a3549b4bec361a44466be-2aae1806a8c9c34e-00","errors":{"id":["The value 'QWER' is not valid."]}}

```

#### +Activities: PUT ​/api​/v1​/Activities​/5
**URL** https://fakerestapi.azurewebsites.net/api/v1/Activities/{{activity_id}}     

**Ожидаемый результат** изменения в 5 id

**Заголовки запроса**
https://fakerestapi.azurewebsites.net/api/v1/Activities/{{activity_id}}

**Тело запроса**

```
{
  "id": 5,
  "title": "string",
  "dueDate": "2023-06-10T12:28:41.698Z",
  "completed": true
}

```

**Заголовки ответа** 
Content-Type: application/json; charset=utf-8; v=1.0
Date: Sun, 11 Jun 2023 05:36:09 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0

**Тело ответа** 

```
{"id":5,"title":"string","dueDate":"2023-06-10T12:28:41.698Z","completed":true}

```

#### id=-1 - PUT ​/api​/v1​/Activities​/{id}
**URL** https://fakerestapi.azurewebsites.net/api/v1/Activities/{{minis_id}}     

**Ожидаемый результат** изменения в id с -1 (ошибка)

**Заголовки запроса**
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 9a191db7-4b7e-4550-b82d-7e0997b9d872
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
{
  "id": -1,
  "title": "string",
  "dueDate": "2023-06-10T12:28:41.698Z",
  "completed": true
}

```

**Заголовки ответа** 
Content-Type: application/json; charset=utf-8; v=1.0
Date: Sun, 11 Jun 2023 05:37:30 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0

**Тело ответа** 

```
{"id":-1,"title":"string","dueDate":"2023-06-10T12:28:41.698Z","completed":true}

```

#### id=LONG - PUT ​/api​/v1​/Activities​/{id}
**URL**   https://fakerestapi.azurewebsites.net/api/v1/Activities/{{long_id}}   

**Ожидаемый результат** изменения в id с длинным значением (ошибка)

**Заголовки запроса**
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: bd25cec8-60d4-43fd-9022-ba51326ec6f7
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
**Тело запроса**

```
{
  "id": 444444444444,
  "title": "string",
  "dueDate": "2023-06-10T12:28:41.698Z",
  "completed": true
}

```

**Заголовки ответа** 
Content-Type: application/problem+json; charset=utf-8
Date: Sun, 11 Jun 2023 05:39:02 GMT
Server: Kestrel
Transfer-Encoding: chunked

**Тело ответа** 

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-d96f2f542c51974a8c2997df96a7f2e6-75dc7ae350118641-00","errors":{"id":["The value '444444444444' is not valid."],"$.id":["The JSON value could not be converted to System.Int32. Path: $.id | LineNumber: 1 | BytePositionInLine: 20."]}}
```


#### id=буквы - PUT ​/api​/v1​/Activities
**URL** https://fakerestapi.azurewebsites.net/api/v1/Activities/{{words_id}}     

**Ожидаемый результат** изменения в id с буквами (ошибка)

**Заголовки запроса**
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 13b746a2-f3bf-43c9-8a5b-b6a3ad66c0d9
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
**Тело запроса**

```
{
  "id": QWER,
  "title": "string",
  "dueDate": "2023-06-10T12:28:41.698Z",
  "completed": true
}

```

**Заголовки ответа** 
Content-Type: application/problem+json; charset=utf-8
Date: Sun, 11 Jun 2023 05:40:25 GMT
Server: Kestrel
Transfer-Encoding: chunked

**Тело ответа** 

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-8c9cba424cdfb84c8abd3548b203a507-15eecb298158d84f-00","errors":{"id":["The value 'QWER' is not valid."],"$.id":["'Q' is an invalid start of a value. Path: $.id | LineNumber: 1 | BytePositionInLine: 8."]}}

```

#### id=пусто - PUT ​/api​/v1​/Activities​/{id}
**URL** https://fakerestapi.azurewebsites.net/api/v1/Activities/{{empty_id}}      

**Ожидаемый результат** изменения в запись с id буквами (ошибка)

**Заголовки запроса**
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 98a86252-9a60-4933-9af7-d49f3ffa8432
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
**Тело запроса**

```
{
  "id": ,
  "title": "string",
  "dueDate": "2023-06-10T12:28:41.698Z",
  "completed": true
}

```

**Заголовки ответа** 
Content-Length: 0
Date: Sun, 11 Jun 2023 05:42:09 GMT
Server: Kestrel
Allow: GET, POST

**Тело ответа** 

```
0
```


#### Тело id=буквы - PUT ​/api​/v1​/Activities​/{id}
**URL** https://fakerestapi.azurewebsites.net/api/v1/Activities/{{activity_id}}     

**Ожидаемый результат** изменения в 5 id тело буквами (ошибка)

**Заголовки запроса**
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 93de51cb-156c-4f64-ae63-4a08a507dc58
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
**Тело запроса**

```
{
  "id": QWER,
  "title": "string",
  "dueDate": "2023-06-10T12:28:41.698Z",
  "completed": true
}
```

**Заголовки ответа** 
Content-Type: application/problem+json; charset=utf-8
Date: Sun, 11 Jun 2023 05:43:46 GMT
Server: Kestrel
Transfer-Encoding: chunked

**Тело ответа** 

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-c27ced13008b424ca6da9ab441e3cb38-da700bc195ea1548-00","errors":{"$.id":["'Q' is an invalid start of a value. Path: $.id | LineNumber: 1 | BytePositionInLine: 8."]}}
```

#### +Activities: DELETE/{id+}
**URL** https://fakerestapi.azurewebsites.net/api/v1/Activities/{{activity_id}}     

**Ожидаемый результат** удаление записи 5

**Заголовки запроса**
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 839cf126-f541-4a7a-b1c7-1adf8fcc5e63
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
**Тело запроса**

```
0
```

**Заголовки ответа** 
Content-Length: 0
Date: Sun, 11 Jun 2023 05:45:00 GMT
Server: Kestrel
api-supported-versions: 1.0

**Тело ответа** 

```
0
```

#### id=LONG - DELETE ​/api​/v1​/Activities​/{id}
**URL** https://fakerestapi.azurewebsites.net/api/v1/Activities/{{long_id}}     

**Ожидаемый результат** удаление записи с длинным id (ошибка)

**Заголовки запроса**
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: efdf1d18-465b-4a3f-af9c-0e43bc1d4316
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
**Тело запроса**

```
0
```

**Заголовки ответа** 
Content-Type: application/problem+json; charset=utf-8
Date: Sun, 11 Jun 2023 05:46:10 GMT
Server: Kestrel
Transfer-Encoding: chunked

**Тело ответа** 

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-0f71e7ec7d6c2c47ad9b81bee3641a43-b49cab4e7f989c47-00","errors":{"id":["The value '444444444444' is not valid."]}}
```

#### id=пусто - DELETE ​/api​/v1​/Activities​/{id}
**URL**  https://fakerestapi.azurewebsites.net/api/v1/Activities/{{empty_id}}    

**Ожидаемый результат** удаление записи с пустым id (ошибка)

**Заголовки запроса**
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 048723a1-567f-44af-8ec4-3f276e2c3799
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
**Тело запроса**

```
0
```

**Заголовки ответа** 
Content-Length: 0
Date: Sun, 11 Jun 2023 05:47:21 GMT
Server: Kestrel
Allow: GET, POST

**Тело ответа** 

```
0
```

#### id=буквы - DELETE ​/api​/v1​/Activities​/{id}
**URL** https://fakerestapi.azurewebsites.net/api/v1/Activities/{{words_id}}     

**Ожидаемый результат** удаление id с буквами (ошибка)

**Заголовки запроса**
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: da1a8f93-e8e4-4cd8-85e5-e31aed97fc70
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
**Тело запроса**

```
0
```

**Заголовки ответа** 
Content-Type: application/problem+json; charset=utf-8
Date: Sun, 11 Jun 2023 05:48:24 GMT
Server: Kestrel
Transfer-Encoding: chunked

**Тело ответа** 

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-8fc082e939069246be5fc7ae8d611ea9-550a8023ec668843-00","errors":{"id":["The value 'QWER' is not valid."]}}
```


#### Authors
#### + GET ​/api​/v1​/Authors
**URL** https://fakerestapi.azurewebsites.net/api/v1/Authors     

**Ожидаемый результат** запрос списка авторов

**Заголовки запроса**
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 52db1e54-fbde-4b75-ae14-f4063f84de8c
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
**Тело запроса**

```
0
```

**Заголовки ответа** 
Content-Type: application/json; charset=utf-8; v=1.0
Date: Sun, 11 Jun 2023 06:01:05 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0

**Тело ответа** 

```
Body is more than 10 KB and can only be viewed in editor. 

```

#### +Authors: DELETE/{id+}
**URL** https://fakerestapi.azurewebsites.net/api/v1/Authors/{{activity_id}}     

**Ожидаемый результат** удаление id

**Заголовки запроса**
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 4ccaef31-f692-414c-a26b-ab442dd3dc75
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
**Тело запроса**

```
0
```

**Заголовки ответа** 
Content-Length: 0
Date: Sun, 11 Jun 2023 06:02:43 GMT
Server: Kestrel
api-supported-versions: 1.0

**Тело ответа** 

```
0
```

#### id=LONG - DELETE ​/api​/v1​/Authors​/{id}
**URL** https://fakerestapi.azurewebsites.net/api/v1/Authors/{{long_id}}     

**Ожидаемый результат** удаление записи с длинным id (ошибка) 

**Заголовки запроса**
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: aa816641-afe9-4df5-94f0-8b2629f4ecee
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
**Тело запроса**

```
0
```

**Заголовки ответа** 
Content-Type: application/problem+json; charset=utf-8
Date: Sun, 11 Jun 2023 06:04:01 GMT
Server: Kestrel
Transfer-Encoding: chunked

**Тело ответа** 

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-234c8c65d16f89458521ed6cf84f6c6d-b5b07b49df881842-00","errors":{"id":["The value '444444444444' is not valid."]}}
```

#### id=пусто - DELETE ​/api​/v1​/Authors​/{id}
**URL** https://fakerestapi.azurewebsites.net/api/v1/Authors/{{empty_id}}     

**Ожидаемый результат** удаление записи с длинным id (ошибка)

**Заголовки запроса**
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 8d378962-3b08-45b5-8eb0-3bf725daab0a
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
**Тело запроса**

```
0
```

**Заголовки ответа** 
Content-Length: 0
Date: Sun, 11 Jun 2023 06:05:10 GMT
Server: Kestrel
Allow: GET, POST

**Тело ответа** 

```
0
```

#### id=буквы - DELETE ​/api​/v1​/Authors​/{id}
**URL** https://fakerestapi.azurewebsites.net/api/v1/Authors/{{words_id}}     

**Ожидаемый результат** удаление записи с id буквами (ошибка)

**Заголовки запроса**
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 4fe9c4d6-c119-4ef3-926f-37d9f1faa1e4
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
**Тело запроса**

```
0
```

**Заголовки ответа** 
Content-Type: application/problem+json; charset=utf-8
Date: Sun, 11 Jun 2023 06:06:27 GMT
Server: Kestrel
Transfer-Encoding: chunked

**Тело ответа** 

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-252d192c19c36f479a110025f900d873-cddab7cd84f78749-00","errors":{"id":["The value 'QWER' is not valid."]}}

```

#### id=5 + Authors GET ​/api​/v1​/Authors
**URL**  https://fakerestapi.azurewebsites.net/api/v1/Authors/{{activity_id}}    

**Ожидаемый результат** запрос данных id 5

**Заголовки запроса**
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 967d65a6-647a-40c2-bcc3-35ada3661d2a
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
**Тело запроса**

```
0
```

**Заголовки ответа** 
Content-Type: application/json; charset=utf-8; v=1.0
Date: Sun, 11 Jun 2023 06:07:43 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0

**Тело ответа** 

```
{"id":5,"idBook":2,"firstName":"First Name 5","lastName":"Last Name 5"}

```

#### id=LONG - GET ​/api​/v1​/Authors
**URL** https://fakerestapi.azurewebsites.net/api/v1/Authors/{{long_id}}     

**Ожидаемый результат** получение записи с длинным id (ошибка)

**Заголовки запроса**
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: ef6aaf8f-d364-4454-b1e7-3a6336c02a9c
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
**Тело запроса**

```
0
```

**Заголовки ответа** 
Content-Type: application/problem+json; charset=utf-8
Date: Sun, 11 Jun 2023 06:08:56 GMT
Server: Kestrel
Transfer-Encoding: chunked

**Тело ответа** 

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-2c86481dabb53442966a15b0e4b8cbb3-a0b91fb26d6a3744-00","errors":{"id":["The value '444444444444' is not valid."]}}

```

#### id=пусто - GET ​/api​/v1​/Authors
**URL** https://fakerestapi.azurewebsites.net/api/v1/Authors/{{empty_id}}      

**Ожидаемый результат** получение данных пустым id (ошибка)

**Заголовки запроса**
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 7c4f10b3-def3-4af7-ad33-a777d897a3d4
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
**Тело запроса**

```
0
```

**Заголовки ответа** 
Content-Type: application/json; charset=utf-8; v=1.0
Date: Sun, 11 Jun 2023 06:10:07 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0

**Тело ответа** 

```
Body is more than 10 KB and can only be viewed in editor
```

#### id=буквы - GET ​/api​/v1​/Authors
**URL** https://fakerestapi.azurewebsites.net/api/v1/Authors/{{words_id}}     

**Ожидаемый результат** получение данных с id буквами (ошибка)

**Заголовки запроса**
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 1a2825f1-774f-449a-b033-c821a3e153d4
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
**Тело запроса**

```
0
```

**Заголовки ответа** 
Content-Type: application/problem+json; charset=utf-8
Date: Sun, 11 Jun 2023 06:11:33 GMT
Server: Kestrel
Transfer-Encoding: chunked

**Тело ответа** 

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-ad652ab27634ee4c8994d44d167c29e5-b2b74bb79fe0e440-00","errors":{"id":["The value 'QWER' is not valid."]}}

```

#### + GET ​/api​/v1​/Authors​/authors​/books​/{idBook}
**URL** https://fakerestapi.azurewebsites.net/api/v1/Authors/authors/books/{{activity_id}}     

**Ожидаемый результат** получение данных по idbook

**Заголовки запроса**
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 1a2825f1-774f-449a-b033-c821a3e153d4
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
**Тело запроса**

```
0
```

**Заголовки ответа** 
Content-Type: application/problem+json; charset=utf-8
Date: Sun, 11 Jun 2023 06:11:33 GMT
Server: Kestrel
Transfer-Encoding: chunked

**Тело ответа** 

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-ad652ab27634ee4c8994d44d167c29e5-b2b74bb79fe0e440-00","errors":{"id":["The value 'QWER' is not valid."]}}
```

#### - GET ​/api​/v1​/Authors​/authors​/books​/{idBook}
**URL** https://fakerestapi.azurewebsites.net/api/v1/Authors/authors/books/{{long_id}}      

**Ожидаемый результат** получение данных по длинному idbook (ошибка)

**Заголовки запроса**
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: d90efd27-027a-4271-ba8f-1369aae0e958
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
**Тело запроса**

```
0
```

**Заголовки ответа** 
Content-Type: application/problem+json; charset=utf-8
Date: Sun, 11 Jun 2023 06:14:06 GMT
Server: Kestrel
Transfer-Encoding: chunked

**Тело ответа** 

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-c141e912ac4d0e4ba086d0e79e002c8f-c53736d6d1bdda4c-00","errors":{"idBook":["The value '444444444444' is not valid."]}}
```

#### idbook=пусто - GET ​/api​/v1​/Authors​/authors​/books​/{idBook}
**URL**  https://fakerestapi.azurewebsites.net/api/v1/Authors/authors/books/{{empty_id}}    

**Ожидаемый результат** получение данных по пустому idbook (ошибка)

**Заголовки запроса**
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: d365b55e-e117-4ac8-9ef8-008eaf627331
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
0
```

**Заголовки ответа** 
Content-Length: 0
Date: Sun, 11 Jun 2023 06:15:26 GMT
Server: Kestrel

**Тело ответа** 

```
0
```

#### id bad -Authors: POST/Request body/444444444444
**URL** https://fakerestapi.azurewebsites.net/api/v1/Authors      

**Ожидаемый результат** добавить запись с длинным id (ошибка)

**Заголовки запроса**
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: f87b5e2e-6c02-4a09-83ee-6ff6e94eb28d
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
{
  "id": 444444444444,
  "idBook": 0,
  "firstName": "string",
  "lastName": "string"
}

```

**Заголовки ответа** 
Content-Type: application/problem+json; charset=utf-8
Date: Sun, 11 Jun 2023 06:16:46 GMT
Server: Kestrel
Transfer-Encoding: chunked

**Тело ответа** 

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-1f120630387f3c4d996d78a4e047b7c0-a8f70a5d86631842-00","errors":{"$.id":["The JSON value could not be converted to System.Int32. Path: $.id | LineNumber: 1 | BytePositionInLine: 20."]}}

```

#### +Authors: PUT ​/api​/v1​/Authors/5
**URL** https://fakerestapi.azurewebsites.net/api/v1/Authors/{{activity_id}}     

**Ожидаемый результат** изменить запись 5

**Заголовки запроса**
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 5481314e-5de8-4efc-a243-fb7fc9957b0c
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
{
  "id": 5,
  "idBook": 0,
  "firstName": "string",
  "lastName": "string"
}

```

**Заголовки ответа** 
Content-Type: application/json; charset=utf-8; v=1.0
Date: Sun, 11 Jun 2023 06:18:05 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0

**Тело ответа** 

```
{"id":5,"idBook":0,"firstName":"string","lastName":"string"}

```

#### id=LONG - PUT ​/api​/v1​/Authors​/{id}
**URL**  https://fakerestapi.azurewebsites.net/api/v1/Authors/{{long_id}}    

**Ожидаемый результат** изменить запись с длинным id

**Заголовки запроса**
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: c03b7f56-1be9-4750-a6f2-b62af8d92ba4
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
{
  "id": 444444444444,
  "idBook": 0,
  "firstName": "string",
  "lastName": "string"
}

```

**Заголовки ответа** 

Content-Type: application/problem+json; charset=utf-8
Date: Sun, 11 Jun 2023 06:19:19 GMT
Server: Kestrel
Transfer-Encoding: chunked

**Тело ответа** 

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-2c20792cd23b7c4f89a41ce1d2dc62bc-b93e788a24a45247-00","errors":{"id":["The value '444444444444' is not valid."],"$.id":["The JSON value could not be converted to System.Int32. Path: $.id | LineNumber: 1 | BytePositionInLine: 20."]}}

```

#### id=пусто - PUT ​/api​/v1​/Authors/{id}
**URL** https://fakerestapi.azurewebsites.net/api/v1/Authors/{{empty_id}}     

**Ожидаемый результат** изменить запись с пустым id (ошибка)

**Заголовки запроса**
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 134f14c8-858a-4b3f-9f12-0e310a49cca1
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
{
  "id": ,
  "idBook": 0,
  "firstName": "string",
  "lastName": "string"
}
```

**Заголовки ответа** 
Content-Length: 0
Date: Sun, 11 Jun 2023 06:20:45 GMT
Server: Kestrel
Allow: GET, POST

**Тело ответа** 

```
0
```

#### id=-1 - Authors PUT ​/api​/v1​/Authors​/{id}
**URL** https://fakerestapi.azurewebsites.net/api/v1/Authors/{{minis_id}}     

**Ожидаемый результат** изменить запись с -1 id (ошибка)

**Заголовки запроса**
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: c7b10958-ac96-430b-b1d1-efd3a72a5872
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
{
  "id": -1,
  "idBook": 0,
  "firstName": "string",
  "lastName": "string"
}
```

**Заголовки ответа** 
Content-Type: application/json; charset=utf-8; v=1.0
Date: Sun, 11 Jun 2023 06:21:57 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0

**Тело ответа** 

```
{"id":-1,"idBook":0,"firstName":"string","lastName":"string"}
```

#### id=буквы - PUT ​/api​/v1​/Authors
**URL** https://fakerestapi.azurewebsites.net/api/v1/Authors/{{words_id}}     

**Ожидаемый результат** внести изм. в id с буквами (ошибка)

**Заголовки запроса**
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 203dca32-e01f-44a3-acd1-53383baa757d
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
{
  "id": QWER,
  "idBook": 0,
  "firstName": "string",
  "lastName": "string"
}
```

**Заголовки ответа** 
Content-Type: application/problem+json; charset=utf-8
Date: Sun, 11 Jun 2023 06:23:17 GMT
Server: Kestrel
Transfer-Encoding: chunked

**Тело ответа** 

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-db6034e20ae04442b828ec7ab33bd28c-5259290721249c45-00","errors":{"id":["The value 'QWER' is not valid."],"$.id":["'Q' is an invalid start of a value. Path: $.id | LineNumber: 1 | BytePositionInLine: 8."]}}
```

#### idbook=пусто - Authors PUT ​/api​/v1​/Authors​/{id}
**URL** https://fakerestapi.azurewebsites.net/api/v1/Authors/{{activity_id}}     

**Ожидаемый результат** изменить запись с пустым idbook (ошибка)

**Заголовки запроса**
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: d01348a9-64a7-4ac9-a382-8cf3ff76cf6e
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
{
  "id": 5,
  "idBook": ,
  "firstName": "string",
  "lastName": "string"
}
```

**Заголовки ответа** 
Content-Type: application/problem+json; charset=utf-8
Date: Sun, 11 Jun 2023 06:24:37 GMT
Server: Kestrel
Transfer-Encoding: chunked

**Тело ответа** 

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-9da4084df0cec944a3e96bf11d757fcb-129bd0208e25ee4a-00","errors":{"$.idBook":["',' is an invalid start of a value. Path: $.idBook | LineNumber: 2 | BytePositionInLine: 12."]}}
```

#### +Authors: POST/Request body/5
**URL** https://fakerestapi.azurewebsites.net/api/v1/Authors      

**Ожидаемый результат** добавить запись 5

**Заголовки запроса**
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 655fafe3-f7fa-4a15-8f79-79467a066f66
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
{
  "id": 5,
  "idBook": 0,
  "firstName": "string",
  "lastName": "string"
}
```

**Заголовки ответа** 
Content-Type: application/json; charset=utf-8; v=1.0
Date: Sun, 11 Jun 2023 06:25:51 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0

**Тело ответа** 

```
{"id":5,"idBook":0,"firstName":"string","lastName":"string"}
```

#### id пусто -Authors: POST/Request body/{id пусто}
**URL** https://fakerestapi.azurewebsites.net/api/v1/Authors     

**Ожидаемый результат** добавить запись с пустым id (ошибка)

**Заголовки запроса**
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 451c0623-4a96-4841-95de-848b20e0e396
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
{
  "id": ,
  "idBook": 0,
  "firstName": "string",
  "lastName": "string"
}
```

**Заголовки ответа** 
Content-Type: application/problem+json; charset=utf-8
Date: Sun, 11 Jun 2023 06:27:09 GMT
Server: Kestrel
Transfer-Encoding: chunked

**Тело ответа** 

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-15b1881453e44149b4c4e9a39590f0a3-4fb60423243cef4c-00","errors":{"$.id":["',' is an invalid start of a value. Path: $.id | LineNumber: 1 | BytePositionInLine: 8."]}}
```

#### id буквы -Authors: POST/Request body/{id QWERT}
**URL** https://fakerestapi.azurewebsites.net/api/v1/Authors     

**Ожидаемый результат** добавить запись с id буквами (ошибка)

**Заголовки запроса**
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 3756fc84-f4d2-47de-af3e-46b10976c5d4
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
{
  "id": QWER,
  "idBook": 0,
  "firstName": "string",
  "lastName": "string"
}
```

**Заголовки ответа** 
Content-Type: application/problem+json; charset=utf-8
Date: Sun, 11 Jun 2023 06:28:25 GMT
Server: Kestrel
Transfer-Encoding: chunked

**Тело ответа** 

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-70c9f7ffeba6274f91bc21b0e1146b4b-cfab7e938967fe42-00","errors":{"$.id":["'Q' is an invalid start of a value. Path: $.id | LineNumber: 1 | BytePositionInLine: 8."]}}
```

#### Тело id=буквы - Authors PUT ​/api​/v1​/Authors/{id}
**URL** https://fakerestapi.azurewebsites.net/api/v1/Authors/{{activity_id}}     

**Ожидаемый результат** изменить запись с id буквами в теле (ошибка)

**Заголовки запроса**
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 3701a7e4-ea2c-4fb3-bebb-1cb6580f65e9
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
{
  "id": QWER,
  "idBook": 0,
  "firstName": "string",
  "lastName": "string"
}
```

**Заголовки ответа** 
Content-Length: 0
Date: Sun, 11 Jun 2023 06:29:37 GMT
Server: Kestrel
Allow: DELETE, GET, PUT

**Тело ответа** 

```
0
```

#### idbook=пусто - Authors POST/Request body/
**URL** https://fakerestapi.azurewebsites.net/api/v1/Authors/{{activity_id}}     

**Ожидаемый результат** добавить запись с пустым idbook (ошибка)

**Заголовки запроса**
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 5ba74d5c-71f6-4a22-abab-8952a365da14
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
{
  "id": 5,
  "idBook": ,
  "firstName": "string",
  "lastName": "string"
}

```

**Заголовки ответа** 

Content-Length: 0
Date: Sun, 11 Jun 2023 06:30:53 GMT
Server: Kestrel
Allow: DELETE, GET, PUT

**Тело ответа** 

```
0
```
#### Books
#### + GET ​/api​/v1​/Books
**URL** https://fakerestapi.azurewebsites.net/api/v1/Books     

**Ожидаемый результат** запрос данных книг

**Заголовки запроса**
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: efdeaf8b-8213-41e4-9d1a-7772cbe73914
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
0
```

**Заголовки ответа** 
Content-Type: application/json; charset=utf-8; v=1.0
Date: Sun, 11 Jun 2023 06:37:43 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0

**Тело ответа** 

```
Body is more than 10 KB and can only be viewed in editor
```

#### id=пусто - GET ​/api​/v1​/Books
**URL** https://fakerestapi.azurewebsites.net/api/v1/Books{{empty_id}}     

**Ожидаемый результат** запрос данных с пустым id 

**Заголовки запроса**
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: ebe2e0e1-bac4-432d-a954-374be4aca6e3
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
0
```

**Заголовки ответа** 
Content-Type: application/json; charset=utf-8; v=1.0
Date: Sun, 11 Jun 2023 06:39:21 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0

**Тело ответа** 

```
Body is more than 10 KB and can only be viewed in editor
```

#### id=пусто - GET ​/api​/v1​/Books
**URL** https://fakerestapi.azurewebsites.net/api/v1/Books{{empty_id}}     

**Ожидаемый результат** запрос данных с пустым id 

**Заголовки запроса**
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 79b93ba0-5b5e-42ab-b741-ddbefb3bb6f1
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
0
```

**Заголовки ответа** 
Content-Type: application/json; charset=utf-8; v=1.0
Date: Sun, 11 Jun 2023 06:41:21 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0

**Тело ответа** 

```
Body is more than 10 KB and can only be viewed in editor
```

#### id=буквы - GET ​/api​/v1​/Books
**URL** https://fakerestapi.azurewebsites.net/api/v1/Books{{words_id}}      

**Ожидаемый результат** запрос данных id пусто (ошибка)

**Заголовки запроса**
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 7818d61e-6015-4538-b434-73f8bf885c5a
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
0
```

**Заголовки ответа** 
Content-Length: 0
Date: Sun, 11 Jun 2023 06:42:34 GMT
Server: Kestrel

**Тело ответа** 

```
0
```

#### +Books: Books/Request body/5
**URL** https://fakerestapi.azurewebsites.net/api/v1/Books/{{activity_id}}     

**Ожидаемый результат** запрос данных по 5

**Заголовки запроса**
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 9949311e-aabc-4e03-bcc1-cf580e584b4c
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
0
```

**Заголовки ответа** 
Content-Type: application/json; charset=utf-8; v=1.0
Date: Sun, 11 Jun 2023 06:43:43 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0

**Тело ответа** 

```
{"id":5,"title":"Book 5","description":"Lorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\n","pageCount":500,"excerpt":"Lorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\n","publishDate":"2023-06-06T06:43:43.397557+00:00"}
```

#### id=LONG - GET ​/api​/v1​/Books
**URL** https://fakerestapi.azurewebsites.net/api/v1/Books/{{long_id}}     

**Ожидаемый результат** запрос данных с длинным id (ошибка)


**Заголовки запроса**
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: bff43175-4c7b-4b70-90b4-e2f308985f7a
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
0
```

**Заголовки ответа** 
Content-Type: application/problem+json; charset=utf-8
Date: Sun, 11 Jun 2023 06:44:58 GMT
Server: Kestrel
Transfer-Encoding: chunked

**Тело ответа** 

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-d4514a09f8801949aac2b528a5fd4d1d-7cd5e0f2d5eed846-00","errors":{"id":["The value '444444444444' is not valid."]}}
```

#### id=4 + Books GET ​/api​/v1​/Authors
**URL** https://fakerestapi.azurewebsites.net/api/v1/Books/4     

**Ожидаемый результат** запрос по 4 id


**Заголовки запроса**
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: f2e15242-afef-48bd-bd83-5901d46fa354
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
0
```

**Заголовки ответа** 
Content-Type: application/json; charset=utf-8; v=1.0
Date: Sun, 11 Jun 2023 06:45:59 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0

**Тело ответа** 

```
{"id":4,"title":"Book 4","description":"Lorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\n","pageCount":400,"excerpt":"Lorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\n","publishDate":"2023-06-07T06:45:59.7735144+00:00"}
```

#### id bad -Books: POST/Request body/444444444444
**URL** https://fakerestapi.azurewebsites.net/api/v1/Books     

**Ожидаемый результат** внести запись с длинным id (ошибка)


**Заголовки запроса**
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: c75cda7c-1351-4b5d-8266-548fdf9e15c1
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
{
  "id": 444444444444,
  "title": "string",
  "description": "string",
  "pageCount": 0,
  "excerpt": "string",
  "publishDate": "2023-06-10T15:01:26.470Z"
}
```

**Заголовки ответа** 
Content-Type: application/problem+json; charset=utf-8
Date: Sun, 11 Jun 2023 06:47:09 GMT
Server: Kestrel
Transfer-Encoding: chunked

**Тело ответа** 

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-ccbdf359b4b2d243a8b1fbabf76e67de-cd7bc84cfe1e0948-00","errors":{"$.id":["The JSON value could not be converted to System.Int32. Path: $.id | LineNumber: 1 | BytePositionInLine: 20."]}}
```

#### id пусто -Books: POST/Request body/{id пусто}
**URL** https://fakerestapi.azurewebsites.net/api/v1/Books     

**Ожидаемый результат** внести запись с пустым id (ошибка)


**Заголовки запроса**
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 28892bee-c2de-4ebf-9add-b1f50cd4c218
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
{
  "id": ,
  "title": "string",
  "description": "string",
  "pageCount": 0,
  "excerpt": "string",
  "publishDate": "2023-06-10T15:01:26.470Z"
}
```

**Заголовки ответа** 
Content-Type: application/problem+json; charset=utf-8
Date: Sun, 11 Jun 2023 06:48:43 GMT
Server: Kestrel
Transfer-Encoding: chunked

**Тело ответа** 

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-5058f82067ac3847825bacecbea37483-7e1934f4ac62d14a-00","errors":{"$.id":["',' is an invalid start of a value. Path: $.id | LineNumber: 1 | BytePositionInLine: 8."]}}

```

#### id буквы -Books: POST/Request body/{id QWERT}
**URL** https://fakerestapi.azurewebsites.net/api/v1/Books     

**Ожидаемый результат** внести запись с id буквами(ошибка)


**Заголовки запроса**
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: ae51bf61-8fda-48a1-acc6-b65881fc212d
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
{
  "id": QWER,
  "title": "string",
  "description": "string",
  "pageCount": 0,
  "excerpt": "string",
  "publishDate": "2023-06-10T15:01:26.470Z"
}
```

**Заголовки ответа** 
Content-Type: application/problem+json; charset=utf-8
Date: Sun, 11 Jun 2023 06:50:00 GMT
Server: Kestrel
Transfer-Encoding: chunked

**Тело ответа** 

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-195bdfa6853fd142ab65d8e486d3eb31-45de29357e4a8c46-00","errors":{"$.id":["'Q' is an invalid start of a value. Path: $.id | LineNumber: 1 | BytePositionInLine: 8."]}}
```

#### +Books: PUT ​/api​/v1​/Books/5
**URL** https://fakerestapi.azurewebsites.net/api/v1/Books/{{activity_id}}     

**Ожидаемый результат** изменения в 5ю 


**Заголовки запроса**
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 2a987759-2af9-4e84-90dd-b3734a0a5c83
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
{
  "id": 5,
  "title": "string",
  "description": "string",
  "pageCount": 0,
  "excerpt": "string",
  "publishDate": "2023-06-10T15:08:14.007Z"
}
```

**Заголовки ответа** 
Content-Type: application/json; charset=utf-8; v=1.0
Date: Sun, 11 Jun 2023 06:51:06 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0

**Тело ответа** 

```
{"id":5,"title":"string","description":"string","pageCount":0,"excerpt":"string","publishDate":"2023-06-10T15:08:14.007Z"}
```

#### id=LONG - PUT ​/api​/v1​/Books/{id}
**URL** https://fakerestapi.azurewebsites.net/api/v1/Books/{{long_id}}     

**Ожидаемый результат** изменения в запись с длиным id (ошибка)

**Заголовки запроса**
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 798fa75c-d013-40b8-af14-93d8f6a64294
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
{
  "id": 444444444444,
  "title": "string",
  "description": "string",
  "pageCount": 0,
  "excerpt": "string",
  "publishDate": "2023-06-10T15:08:14.007Z"
}
```

**Заголовки ответа** 
Content-Type: application/problem+json; charset=utf-8
Date: Sun, 11 Jun 2023 06:52:24 GMT
Server: Kestrel
Transfer-Encoding: chunked

**Тело ответа** 

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-d3bf82309f802047b3c4b97bcc111d94-a8315f90b706d241-00","errors":{"id":["The value '444444444444' is not valid."],"$.id":["The JSON value could not be converted to System.Int32. Path: $.id | LineNumber: 1 | BytePositionInLine: 20."]}}
```

#### id=пусто - PUT ​/api​/v1​/Books/{id}
**URL** https://fakerestapi.azurewebsites.net/api/v1/Books/{{empty_id}}      

**Ожидаемый результат** изменения в запись с пустым id (ошибка)


**Заголовки запроса**
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 66eb2c53-f2be-4402-8d3e-603123cbff02
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
{
  "id": ,
  "title": "string",
  "description": "string",
  "pageCount": 0,
  "excerpt": "string",
  "publishDate": "2023-06-10T15:08:14.007Z"
}
```

**Заголовки ответа** 
Content-Length: 0
Date: Sun, 11 Jun 2023 06:53:42 GMT
Server: Kestrel
Allow: GET, POST

**Тело ответа** 

```
0
```

#### id=буквы - PUT ​/api​/v1​/Books
**URL** https://fakerestapi.azurewebsites.net/api/v1/Books/{{words_id}}     

**Ожидаемый результат** внести изменения в запись с id буквами (ошибка)


**Заголовки запроса**
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 6ede291f-0489-4313-b133-eb1484f2cba1
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
{
  "id": QWER,
  "title": "string",
  "description": "string",
  "pageCount": 0,
  "excerpt": "string",
  "publishDate": "2023-06-10T15:08:14.007Z"
}

```

**Заголовки ответа** 
Content-Type: application/problem+json; charset=utf-8
Date: Sun, 11 Jun 2023 06:54:51 GMT
Server: Kestrel
Transfer-Encoding: chunked

**Тело ответа** 

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-218d64600005394ba1431106af1c843a-96d5810b3b820a49-00","errors":{"id":["The value 'QWER' is not valid."],"$.id":["'Q' is an invalid start of a value. Path: $.id | LineNumber: 1 | BytePositionInLine: 8."]}}

```

#### Тело id=буквы - Books PUT ​/api​/v1​/Books/{id}
**URL** https://fakerestapi.azurewebsites.net/api/v1/Books/{{activity_id}}     

**Ожидаемый результат** внести изменнеия в 5 в теле id буквами (ошибка)


**Заголовки запроса**
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: fc7f86b6-f84e-45fb-ac3e-4a458a2a3e7b
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
{
  "id": QWER,
  "title": "string",
  "description": "string",
  "pageCount": 0,
  "excerpt": "string",
  "publishDate": "2023-06-10T15:08:14.007Z"
}
```

**Заголовки ответа** 
Content-Type: application/problem+json; charset=utf-8
Date: Sun, 11 Jun 2023 06:56:19 GMT
Server: Kestrel
Transfer-Encoding: chunked

**Тело ответа** 

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-58f16e0061dca44f972d84a708233b6a-d16179f654c1414b-00","errors":{"$.id":["'Q' is an invalid start of a value. Path: $.id | LineNumber: 1 | BytePositionInLine: 8."]}}
```

#### Title=пусто - Books PUT ​/api​/v1​/Books​/{id}
**URL** https://fakerestapi.azurewebsites.net/api/v1/Books/{{activity_id}}     

**Ожидаемый результат** внести изменения в 5 поле title пусто (ошибка)


**Заголовки запроса**
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: a57ceae9-1e13-431d-9bce-90686eca9701
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
{
  "id": 5,
  "title": ,
  "description": "string",
  "pageCount": 0,
  "excerpt": "string",
  "publishDate": "2023-06-10T15:08:14.007Z"
}
```

**Заголовки ответа** 
Content-Type: application/problem+json; charset=utf-8
Date: Sun, 11 Jun 2023 06:57:33 GMT
Server: Kestrel
Transfer-Encoding: chunked

**Тело ответа** 

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-e94e989cebc2bf4db786e74e5ef127ce-1e3c94b12156094e-00","errors":{"$.title":["',' is an invalid start of a value. Path: $.title | LineNumber: 2 | BytePositionInLine: 11."]}}
```

#### id=-1 - Books PUT ​/api​/v1​/Books​/{id}
**URL** https://fakerestapi.azurewebsites.net/api/v1/Books/{{minis_id}}     

**Ожидаемый результат** внести изм. в запись с id -1 (ошибка)


**Заголовки запроса**
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 36c0d992-d37b-4561-a2ab-c363755449f1
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
{
  "id": -1,
  "title": "string",
  "description": "string",
  "pageCount": 0,
  "excerpt": "string",
  "publishDate": "2023-06-10T15:08:14.007Z"
}
```

**Заголовки ответа** 
Content-Type: application/json; charset=utf-8; v=1.0
Date: Sun, 11 Jun 2023 06:58:48 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0

**Тело ответа** 

```
{"id":-1,"title":"string","description":"string","pageCount":0,"excerpt":"string","publishDate":"2023-06-10T15:08:14.007Z"}
```

#### Title=пусто - Books POST/Request body/
**URL** https://fakerestapi.azurewebsites.net/api/v1/Books/     

**Ожидаемый результат** добавить запись 5 с пустым title (ошибка)


**Заголовки запроса**
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: b1b55c5d-c668-4bf3-a510-202d10ae9648
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
{
  "id": 5,
  "title": QWER,
  "description": "string",
  "pageCount": 0,
  "excerpt": "string",
  "publishDate": "2023-06-10T15:01:26.470Z"
}
```

**Заголовки ответа** 
Content-Type: application/problem+json; charset=utf-8
Date: Sun, 11 Jun 2023 07:00:03 GMT
Server: Kestrel
Transfer-Encoding: chunked

**Тело ответа** 

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-13883f982148a9498caa144e8ff14878-3d08deb245c73c45-00","errors":{"$.title":["'Q' is an invalid start of a value. Path: $.title | LineNumber: 2 | BytePositionInLine: 11."]}}
```

#### +Books: DELETE/{id+}
**URL** https://fakerestapi.azurewebsites.net/api/v1/Books/{{activity_id}}     

**Ожидаемый результат** удалить запись 5


**Заголовки запроса**
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: cbfe39c9-5ee6-402c-9014-09a8b3d26374
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
0
```

**Заголовки ответа** 
Content-Length: 0
Date: Sun, 11 Jun 2023 07:01:01 GMT
Server: Kestrel
api-supported-versions: 1.0

**Тело ответа** 

```
0
```

#### id=LONG - DELETE ​/api​/v1​/Books​/{id}
**URL** https://fakerestapi.azurewebsites.net/api/v1/Books/{{long_id}}     

**Ожидаемый результат** удалить запись с длинным id (ошибка)


**Заголовки запроса**
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 23e87840-6b02-4822-8dc0-53f9a1631f2b
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
0
```

**Заголовки ответа** 
Content-Type: application/problem+json; charset=utf-8
Date: Sun, 11 Jun 2023 07:02:06 GMT
Server: Kestrel
Transfer-Encoding: chunked

**Тело ответа** 

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-dcdac49c9a5e434988db1f8698590417-c172b8ae94c72b4d-00","errors":{"id":["The value '444444444444' is not valid."]}}

```

#### id=пусто - DELETE ​/api​/v1​/Books​/{id}
**URL** https://fakerestapi.azurewebsites.net/api/v1/Books/{{empty_id}}     

**Ожидаемый результат** удалить запись с пустым id ошибка


**Заголовки запроса**
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 4d2657e4-0442-444f-97f1-5442f5643b2a
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
0
```

**Заголовки ответа** 
Content-Length: 0
Date: Sun, 11 Jun 2023 07:03:16 GMT
Server: Kestrel
Allow: GET, POST

**Тело ответа** 

```
0
```

#### id=буквы - DELETE ​/api​/v1​/Books​/{id}
**URL** https://fakerestapi.azurewebsites.net/api/v1/Books/{{words_id}}     

**Ожидаемый результат** удалить запись с id буквами ошибка


**Заголовки запроса**
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: e71b8b06-b452-4d8a-b83b-2dd749edbdec
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
0
```

**Заголовки ответа** 
Content-Type: application/problem+json; charset=utf-8
Date: Sun, 11 Jun 2023 07:04:24 GMT
Server: Kestrel
Transfer-Encoding: chunked

**Тело ответа** 

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-92db4a9443fd6e45aa3f795d88458e21-710ddd3387f7ea43-00","errors":{"id":["The value 'QWER' is not valid."]}}
```

# CoverPhotos
#### GET ​/api​/v1​/CoverPhotos
**URL** https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos     

**Ожидаемый результат** получить данные


**Заголовки запроса**
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 1c9cea9e-e67a-4953-86d0-0dc6cc37ee49
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
0
```

**Заголовки ответа** 
Content-Type: application/json; charset=utf-8; v=1.0
Date: Sun, 11 Jun 2023 07:06:19 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0

**Тело ответа** 

```
Body is more than 10 KB and can only be viewed in editor
```

#### id bad -CoverPhotos: GET/Request body/
**URL** https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos{{long_id}}     

**Ожидаемый результат** получение данных с длинным id (ошибка)


**Заголовки запроса**
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: cd0962ea-5ce2-450b-be38-7ade6114ccdb
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
0
```

**Заголовки ответа** 
Content-Length: 0
Date: Sun, 11 Jun 2023 07:07:50 GMT
Server: Kestrel

**Тело ответа** 

```
0
```

#### id=LONG - GET ​/api​/v1​/CoverPhotos
**URL** https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos{{long_id}}      

**Ожидаемый результат** получение данных с плохим id (ошибка)


**Заголовки запроса**
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 26712e35-a648-499e-93f7-f6cda90cbc36
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
0
```

**Заголовки ответа** 
Content-Length: 0
Date: Sun, 11 Jun 2023 07:09:38 GMT
Server: Kestrel

**Тело ответа** 

```
0
```

#### id=4 + CoverPhotos GET ​/api​/v1​/CoverPhotos
**URL** https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/{{activity_id}}     

**Ожидаемый результат** получение записи 5


**Заголовки запроса**
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 5f94ae48-ff2a-4a46-9f57-2c96d3d78850
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
0
```

**Заголовки ответа** 
Content-Type: application/json; charset=utf-8; v=1.0
Date: Sun, 11 Jun 2023 07:10:38 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0

**Тело ответа** 

```
{"id":5,"idBook":5,"url":"https://placeholdit.imgix.net/~text?txtsize=33&txt=Book 5&w=250&h=350"}
```

#### id=пусто - GET ​/api​/v1​/CoverPhotos
**URL** https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/{{empty_id}}     

**Ожидаемый результат** получение записи с пустым id


**Заголовки запроса**
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 7a9f2821-908f-46a6-8ba8-6c1bbfaaa98f
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
0
```

**Заголовки ответа** 
Content-Type: application/json; charset=utf-8; v=1.0
Date: Sun, 11 Jun 2023 07:11:46 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0

**Тело ответа** 

```
Body is more than 10 KB and can only be viewed in editor
```

#### id=буквы - GET ​/api​/v1​/CoverPhotos
**URL** https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/{{words_id}}     

**Ожидаемый результат** получение записи с id буквами ошибка


**Заголовки запроса**
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 7cbc10c5-cb4e-4641-84c7-9f2365c412df
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
0
```

**Заголовки ответа** 
Content-Type: application/problem+json; charset=utf-8
Date: Sun, 11 Jun 2023 07:12:56 GMT
Server: Kestrel
Transfer-Encoding: chunked

**Тело ответа** 

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-fdd1f8df25186f41bd46c3d5138c50e2-54d4f8164b544e42-00","errors":{"id":["The value 'QWER' is not valid."]}}
```

#### +GET ​/api​/v1​/CoverPhotos​/books​/covers​/{idBook}
**URL** https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/books/covers/{{activity_id}}     

**Ожидаемый результат** получение записи по idbook


**Заголовки запроса**
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 7f259a47-8974-4835-a182-5fd950af516a
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
0
```

**Заголовки ответа** 
Content-Type: application/json; charset=utf-8; v=1.0
Date: Sun, 11 Jun 2023 07:13:56 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0

**Тело ответа** 

```
[{"id":5,"idBook":5,"url":"https://placeholdit.imgix.net/~text?txtsize=33&txt=Book 5&w=250&h=350"}]
```

#### - GET ​/api​/v1​/CoverPhotos​/books​/covers​/{idBook}
**URL** https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/books/covers/{{long_id}}     

**Ожидаемый результат** получение записи с плохим idbook ошибка


**Заголовки запроса**
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 47278704-4d06-488f-a853-93c13485a528
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
0
```

**Заголовки ответа** 
Content-Type: application/problem+json; charset=utf-8
Date: Sun, 11 Jun 2023 07:15:18 GMT
Server: Kestrel
Transfer-Encoding: chunked

**Тело ответа** 

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-cb2701b343cb6242869f48245916c5cc-f7d4ae3c88888a44-00","errors":{"idBook":["The value '444444444444' is not valid."]}}
```

#### +CoverPhotos: DELETE/{id+}
**URL** https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/{{activity_id}}     

**Ожидаемый результат** удалить запись с 5


**Заголовки запроса**
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 52d44579-c844-44c9-860a-e4d0ddd0ffe2
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```

```

**Заголовки ответа** 
Content-Length: 0
Date: Sun, 11 Jun 2023 07:16:13 GMT
Server: Kestrel
api-supported-versions: 1.0

**Тело ответа** 

```
0
```

#### id=LONG - DELETE ​/api​/v1​/CoverPhotos​/{id}
**URL** https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/{{long_id}}     

**Ожидаемый результат** удалить запись с длинным id (ошибка) 


**Заголовки запроса**
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 19fe1c59-e80e-4955-8c62-7b735a4f9a26
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
0
```

**Заголовки ответа** 
Content-Type: application/problem+json; charset=utf-8
Date: Sun, 11 Jun 2023 07:17:25 GMT
Server: Kestrel
Transfer-Encoding: chunked

**Тело ответа** 

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-4463360be0870d4fa3727ab845679baf-3c2e76c147259f43-00","errors":{"id":["The value '444444444444' is not valid."]}}
```

#### id=пусто - DELETE ​/api​/v1​/CoverPhotos​/{id}
**URL** https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/{{empty_id}}     

**Ожидаемый результат** удалить запись с пустым id ошибка


**Заголовки запроса**
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: f6e4076d-5299-4e0e-9632-29a39bf8c82d
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```

```

**Заголовки ответа** 
Content-Length: 0
Date: Sun, 11 Jun 2023 07:18:25 GMT
Server: Kestrel
Allow: GET, POST

**Тело ответа** 

```
0
```

#### id=буквы - DELETE ​/api​/v1​/CoverPhotos​/{id}
**URL** https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/{{words_id}}     

**Ожидаемый результат** удалить запись с id буквами ошибка


**Заголовки запроса**
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 0f31fe00-9e61-44bb-b42a-38949fe332e1
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```

```

**Заголовки ответа** 
Content-Type: application/problem+json; charset=utf-8
Date: Sun, 11 Jun 2023 07:19:30 GMT
Server: Kestrel
Transfer-Encoding: chunked

**Тело ответа** 

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-bce75034a2f81844b86ac4031e44d7bf-467bbab3b3c47f40-00","errors":{"id":["The value 'QWER' is not valid."]}}
```

#### +CoverPhotos: POST/Request body/5
**URL** https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos     

**Ожидаемый результат** внести запись 5


**Заголовки запроса**
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: fff3d048-540d-4049-b8bb-02902752ca65
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
{
  "id": 5,
  "idBook": 0,
  "url": "string"
}
```

**Заголовки ответа** 
Content-Type: application/json; charset=utf-8; v=1.0
Date: Sun, 11 Jun 2023 07:20:34 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0

**Тело ответа** 

```
{"id":5,"idBook":0,"url":"string"}
```

#### id пусто -CoverPhotos: POST/Request body/{id пусто}
**URL** https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos      

**Ожидаемый результат** внести запись с пустым id ошибка


**Заголовки запроса**
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: ca386e53-966b-4c7b-844a-4505cfb5cdf4
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
{
  "id": ,
  "idBook": 0,
  "url": "string"
}
```

**Заголовки ответа** 
Content-Type: application/problem+json; charset=utf-8
Date: Sun, 11 Jun 2023 07:21:51 GMT
Server: Kestrel
Transfer-Encoding: chunked

**Тело ответа** 

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-b3f82b5ab4c39a4cbbea4a8004f23d6a-a8f4854eced9694e-00","errors":{"$.id":["',' is an invalid start of a value. Path: $.id | LineNumber: 1 | BytePositionInLine: 8."]}}
```

#### id буквы -CoverPhotos: POST/Request body/{id QWERT}
**URL** https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos     

**Ожидаемый результат** внести запись с id буквами ошибка


**Заголовки запроса**
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 500afb5a-b917-4eda-83c9-a90bdb3440d2
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
{
  "id": QWER,
  "idBook": 0,
  "url": "string"
}
```

**Заголовки ответа** 
Content-Type: application/problem+json; charset=utf-8
Date: Sun, 11 Jun 2023 07:23:00 GMT
Server: Kestrel
Transfer-Encoding: chunked

**Тело ответа** 

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-cb8a9e0a335b6643b0f394e1a212ddc8-b633dc1be9b6d04f-00","errors":{"$.id":["'Q' is an invalid start of a value. Path: $.id | LineNumber: 1 | BytePositionInLine: 8."]}}
```

#### idBook=пусто - CoverPhotos POST/Request body/
**URL** https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/books/covers/{{empty_id}}     

**Ожидаемый результат** добавить запись с пустым idbook ошибка 


**Заголовки запроса**
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: b869ac36-426c-46f1-b30e-6e2d2840e650
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
{
  "id": QWER,
  "idBook": 0,
  "url": "string"
}
```

**Заголовки ответа** 
Content-Length: 0
Date: Sun, 11 Jun 2023 07:24:50 GMT
Server: Kestrel

**Тело ответа** 

```
0
```

#### + PUT ​/api​/v1​/CoverPhotos/5
**URL** https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/{{activity_id}}     

**Ожидаемый результат** изменить запись 5


**Заголовки запроса**
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: e800fe9f-c046-4c88-95ed-5b2657446204
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
{
  "id": 5,
  "idBook": 0,
  "url": "string"
}
```

**Заголовки ответа** 
Content-Type: application/json; charset=utf-8; v=1.0
Date: Sun, 11 Jun 2023 07:25:46 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0

**Тело ответа** 

```
{"id":5,"idBook":0,"url":"string"}
```

#### id=LONG - PUT ​/api​/v1​/CoverPhotos​/{id}
**URL** https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/{{long_id}}     

**Ожидаемый результат** внести изменения в запись с длинным id (ошибка) 


**Заголовки запроса**
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: cc2d4687-7e94-4130-a7e1-890855be5798
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
{
  "id": 444444444444,
  "idBook": 0,
  "url": "string"
}
```

**Заголовки ответа** 
Content-Type: application/problem+json; charset=utf-8
Date: Sun, 11 Jun 2023 07:33:49 GMT
Server: Kestrel
Transfer-Encoding: chunked

**Тело ответа** 

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-0a7f0d81829ab44d9dadc75e90be15a4-40f68cf5812f4a4b-00","errors":{"id":["The value '444444444444' is not valid."],"$.id":["The JSON value could not be converted to System.Int32. Path: $.id | LineNumber: 1 | BytePositionInLine: 20."]}}
```

#### id=пусто - PUT ​/api​/v1​/CoverPhotos/{id}
**URL** https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/{{empty_id}}     

**Ожидаемый результат** внести изменения с пустым id ошибка 


**Заголовки запроса**
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: f4a4943b-c7dc-4f34-bf51-b68cc5d6341a
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
{
  "id": ,
  "idBook": 0,
  "url": "string"
}
```

**Заголовки ответа** 
Content-Length: 0
Date: Sun, 11 Jun 2023 07:35:06 GMT
Server: Kestrel
Allow: GET, POST

**Тело ответа** 

```
0
```

#### id=буквы - PUT ​/api​/v1​/CoverPhotos
**URL** https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/{{words_id}}     

**Ожидаемый результат** внести изменения с id буквами ошибка 


**Заголовки запроса**
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 1fe32f2c-d6b3-41f7-8519-0c861c681455
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
{
  "id": QWER,
  "idBook": 0,
  "url": "string"
}
```

**Заголовки ответа** 
Content-Type: application/problem+json; charset=utf-8
Date: Sun, 11 Jun 2023 07:36:21 GMT
Server: Kestrel
Transfer-Encoding: chunked

**Тело ответа** 

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-5139d3ac86b2754f9d32d808be5c1459-8ea7e7c81f30bd45-00","errors":{"id":["The value 'QWER' is not valid."],"$.id":["'Q' is an invalid start of a value. Path: $.id | LineNumber: 1 | BytePositionInLine: 8."]}}
```

#### Тело id=буквы - CoverPhotos PUT ​/api​/v1​/CoverPhotos​/{id}
**URL** https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/{{activity_id}}     

**Ожидаемый результат** внести изменения в запись 5 с id в теле буквами ошибка


**Заголовки запроса**
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 833f7231-1473-4559-bbca-4e7652625aa9
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
{
  "id": QWER,
  "idBook": 0,
  "url": "string"
}
```

**Заголовки ответа** 
Content-Type: application/problem+json; charset=utf-8
Date: Sun, 11 Jun 2023 07:37:52 GMT
Server: Kestrel
Transfer-Encoding: chunked

**Тело ответа** 

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-b4ce863295e04443ac6689be4c46efd6-313ba8a2b8b8c444-00","errors":{"$.id":["'Q' is an invalid start of a value. Path: $.id | LineNumber: 1 | BytePositionInLine: 8."]}}
```

#### idBook=пусто - CoverPhotos PUT ​/api​/v1​/CoverPhotos​/{id}
**URL** https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/books/covers/{{empty_id}}     

**Ожидаемый результат** внести изменения в запись с idbook пустым ошибка


**Заголовки запроса**
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 52c7cdd1-0301-4857-9679-81106f45327c
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
{
  "id": ,
  "idBook": 0,
  "url": "string"
}
```

**Заголовки ответа** 
Content-Length: 0
Date: Sun, 11 Jun 2023 07:39:15 GMT
Server: Kestrel

**Тело ответа** 

```
0
```

#### id=-1 - PUT ​/api​/v1​/CoverPhotos​/{id}
**URL** https://fakerestapi.azurewebsites.net/api/v1/CoverPhotos/{{minis_id}}     

**Ожидаемый результат** внести изменения в запись с id -1 ошибка


**Заголовки запроса**
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: e1023843-6eea-4756-b6f5-c4a196ba6370
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
{
  "id": -1,
  "idBook": 0,
  "url": "string"
}
```

**Заголовки ответа** 
Content-Type: application/json; charset=utf-8; v=1.0
Date: Sun, 11 Jun 2023 07:40:26 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0

**Тело ответа** 

```
{"id":-1,"idBook":0,"url":"string"}
```

#### Users
#### + GET ​/api​/v1​/Users
**URL** https://fakerestapi.azurewebsites.net/api/v1/Users     

**Ожидаемый результат** получение спика юзеров


**Заголовки запроса**
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 39e7a488-85f8-49c3-977b-1184b757e699
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
0
```

**Заголовки ответа** 
Content-Type: application/json; charset=utf-8; v=1.0
Date: Sun, 11 Jun 2023 07:41:49 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0

**Тело ответа** 

```
[{"id":1,"userName":"User 1","password":"Password1"},{"id":2,"userName":"User 2","password":"Password2"},{"id":3,"userName":"User 3","password":"Password3"},{"id":4,"userName":"User 4","password":"Password4"},{"id":5,"userName":"User 5","password":"Password5"},{"id":6,"userName":"User 6","password":"Password6"},{"id":7,"userName":"User 7","password":"Password7"},{"id":8,"userName":"User 8","password":"Password8"},{"id":9,"userName":"User 9","password":"Password9"},{"id":10,"userName":"User 10","password":"Password10"}]
```

#### id bad -Users: GET/Request body/
**URL** https://fakerestapi.azurewebsites.net/api/v1/Users/{{long_id}}     

**Ожидаемый результат** получить запись с длинным id ошибка


**Заголовки запроса**
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: ec377ea8-70f0-4dfc-90fa-5a44c01cf7ac
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
0
```

**Заголовки ответа** 
Content-Type: application/problem+json; charset=utf-8
Date: Sun, 11 Jun 2023 07:42:59 GMT
Server: Kestrel
Transfer-Encoding: chunked

**Тело ответа** 

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-bb5bd345eb688e49a7f2d1a1adf32298-72f1b43b481e704f-00","errors":{"id":["The value '444444444444' is not valid."]}}
```

#### id=5 + Users GET ​/api​/v1​/Users
**URL** https://fakerestapi.azurewebsites.net/api/v1/Users/{{activity_id}}      

**Ожидаемый результат** получить запись 5


**Заголовки запроса**
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 2cd97627-7435-4485-b2bc-cab7335ea267
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
0
```

**Заголовки ответа** 
Content-Type: application/json; charset=utf-8; v=1.0
Date: Sun, 11 Jun 2023 07:44:55 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0

**Тело ответа** 

```
{"id":5,"userName":"User 5","password":"Password5"}
```

#### id=LONG - GET ​/api​/v1​/Users
**URL** https://fakerestapi.azurewebsites.net/api/v1/Users/{{long_id}}     

**Ожидаемый результат** получить запись с длинным id ошибка


**Заголовки запроса**
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: e6db84ad-26d2-4755-ae47-6f93ef569834
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
0
```

**Заголовки ответа** 
Content-Type: application/problem+json; charset=utf-8
Date: Sun, 11 Jun 2023 07:45:53 GMT
Server: Kestrel
Transfer-Encoding: chunked

**Тело ответа** 

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-69f626557bc84144b8079fef587c62f9-842f95f9175f7043-00","errors":{"id":["The value '444444444444' is not valid."]}}
```

#### id=пусто - GET ​/api​/v1​/Users
**URL** https://fakerestapi.azurewebsites.net/api/v1/Users/{{empty_id}}     

**Ожидаемый результат** получить запись с пустым id 


**Заголовки запроса**
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: b4ccefb3-fe76-45f9-9f8d-9f2b4e349188
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
0
```

**Заголовки ответа** 
Content-Type: application/json; charset=utf-8; v=1.0
Date: Sun, 11 Jun 2023 07:47:31 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0

**Тело ответа** 

```
[{"id":1,"userName":"User 1","password":"Password1"},{"id":2,"userName":"User 2","password":"Password2"},{"id":3,"userName":"User 3","password":"Password3"},{"id":4,"userName":"User 4","password":"Password4"},{"id":5,"userName":"User 5","password":"Password5"},{"id":6,"userName":"User 6","password":"Password6"},{"id":7,"userName":"User 7","password":"Password7"},{"id":8,"userName":"User 8","password":"Password8"},{"id":9,"userName":"User 9","password":"Password9"},{"id":10,"userName":"User 10","password":"Password10"}]
```

#### id=буквы - GET ​/api​/v1​/Users
**URL** https://fakerestapi.azurewebsites.net/api/v1/Users/{{words_id}}     

**Ожидаемый результат** получить запись с id буквами ошибка


**Заголовки запроса**
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: e41f587a-d1f7-4a85-88fe-731a5375803c
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
0
```

**Заголовки ответа** 
Content-Type: application/problem+json; charset=utf-8
Date: Sun, 11 Jun 2023 07:48:36 GMT
Server: Kestrel
Transfer-Encoding: chunked

**Тело ответа** 

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-344ae229325499499e7468daa969c73e-4b378b5953218145-00","errors":{"id":["The value 'QWER' is not valid."]}}
```

#### +Users: DELETE/{id+}
**URL** https://fakerestapi.azurewebsites.net/api/v1/Users/{{activity_id}}     

**Ожидаемый результат** удалить запись 5


**Заголовки запроса**
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 1081c03e-2d09-455e-9667-c10a4f9dfb04
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
0
```

**Заголовки ответа** 
Content-Length: 0
Date: Sun, 11 Jun 2023 07:49:40 GMT
Server: Kestrel
api-supported-versions: 1.0

**Тело ответа** 

```
0
```

#### id=LONG - DELETE ​/api​/v1​/Users​/{id}
**URL** https://fakerestapi.azurewebsites.net/api/v1/Users/{{long_id}}     

**Ожидаемый результат** удалить запись с длинным id ошибка


**Заголовки запроса**
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 94e6bc5b-1103-4751-860a-f45539dd4f42
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
0
```

**Заголовки ответа** 
Content-Type: application/problem+json; charset=utf-8
Date: Sun, 11 Jun 2023 07:50:41 GMT
Server: Kestrel
Transfer-Encoding: chunked

**Тело ответа** 

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-83db4a256c40e14da4f80e0ab69ab072-e01b63043cdd534b-00","errors":{"id":["The value '444444444444' is not valid."]}}
```

#### id=пусто - DELETE ​/api​/v1​/Users​/{id}
**URL** https://fakerestapi.azurewebsites.net/api/v1/Users/{{empty_id}}     

**Ожидаемый результат** удалить запись с пустым id ошибка


**Заголовки запроса**
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 438a8316-4a88-4c1d-90c3-50264aafd5a3
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
0
```

**Заголовки ответа** 
Content-Length: 0
Date: Sun, 11 Jun 2023 07:51:46 GMT
Server: Kestrel
Allow: GET, POST

**Тело ответа** 

```
0
```

#### id=буквы - DELETE ​/api​/v1​/Users​/{id}
**URL** https://fakerestapi.azurewebsites.net/api/v1/Users/{{words_id}}     

**Ожидаемый результат** удалить запись с id буквами ошибка


**Заголовки запроса**
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: c16c15af-0dab-4ca5-8325-0aac8ff946f9
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
0
```

**Заголовки ответа** 
Content-Type: application/problem+json; charset=utf-8
Date: Sun, 11 Jun 2023 07:52:52 GMT
Server: Kestrel
Transfer-Encoding: chunked

**Тело ответа** 

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-6aef194f6cd43a4bab931a5c55d92930-7652f0fd36331d44-00","errors":{"id":["The value 'QWER' is not valid."]}}
```

#### +Users: PUT ​/api​/v1​/Users/5
**URL** https://fakerestapi.azurewebsites.net/api/v1/Users/{{activity_id}}      

**Ожидаемый результат** внести изменения в 5 запись


**Заголовки запроса**
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 7821f329-9f51-42b2-92e6-e81617aaba81
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
{
  "id": 5,
  "userName": "string",
  "password": "string"
}
```

**Заголовки ответа** 
Content-Type: application/json; charset=utf-8; v=1.0
Date: Sun, 11 Jun 2023 07:53:57 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0

**Тело ответа** 

```
{"id":5,"userName":"string","password":"string"}
```

#### id=LONG - PUT ​/api​/v1​/Users​/{id}
**URL** https://fakerestapi.azurewebsites.net/api/v1/Users/{{long_id}}     

**Ожидаемый результат** внести изменения в запись с длинным id ошибка


**Заголовки запроса**
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 5fc8e0f3-151e-4373-a19a-6de55c8273d8
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
{
  "id": 444444444444,
  "userName": "string",
  "password": "string"
}
```

**Заголовки ответа** 
Content-Type: application/problem+json; charset=utf-8
Date: Sun, 11 Jun 2023 07:55:11 GMT
Server: Kestrel
Transfer-Encoding: chunked

**Тело ответа** 

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-0a4270ea5814624687cc116255b11f26-2745afef72360d49-00","errors":{"id":["The value '444444444444' is not valid."],"$.id":["The JSON value could not be converted to System.Int32. Path: $.id | LineNumber: 1 | BytePositionInLine: 20."]}}
```

#### Тело id=буквы - Users PUT ​/api​/v1​/Users/{id}
**URL** https://fakerestapi.azurewebsites.net/api/v1/Users/{{activity_id}}     

**Ожидаемый результат** внести изменения в запись 5 в теле id буквы ошибка


**Заголовки запроса**
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 48c36874-46b5-4639-b0af-1d0ac0bbc2d6
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
{
  "id": QWER,
  "userName": "string",
  "password": "string"
}
```

**Заголовки ответа** 
Content-Type: application/problem+json; charset=utf-8
Date: Sun, 11 Jun 2023 07:56:28 GMT
Server: Kestrel
Transfer-Encoding: chunked

**Тело ответа** 

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-64f1d9e9e0b64244829ef0550e9760d4-7a203b33597f2b41-00","errors":{"$.id":["'Q' is an invalid start of a value. Path: $.id | LineNumber: 1 | BytePositionInLine: 8."]}}
```

#### id=пусто - PUT ​/api​/v1​/Users/{id}
**URL** https://fakerestapi.azurewebsites.net/api/v1/Users/{{empty_id}}     

**Ожидаемый результат** внести изм в запись с пустым id ошибка


**Заголовки запроса**
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: ac0fe777-fc58-4b01-9129-d576771d922f
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
{
  "id": ,
  "userName": "string",
  "password": "string"
}
```

**Заголовки ответа** 
Content-Length: 0
Date: Sun, 11 Jun 2023 07:57:36 GMT
Server: Kestrel
Allow: GET, POST

**Тело ответа** 

```
0
```

#### id=буквы - PUT ​/api​/v1​/Users
**URL** https://fakerestapi.azurewebsites.net/api/v1/Users/{{words_id}}     

**Ожидаемый результат** внести изменения в запись где id буквы ошибка


**Заголовки запроса**
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 6e927af8-888c-40b9-a25e-19bb75017a6e
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
{
  "id": QWER,
  "userName": "string",
  "password": "string"
}
```

**Заголовки ответа** 
Content-Type: application/problem+json; charset=utf-8
Date: Sun, 11 Jun 2023 07:58:48 GMT
Server: Kestrel
Transfer-Encoding: chunked

**Тело ответа** 

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-5417355336be5641b2e70429371966ef-237a6cae77b56940-00","errors":{"id":["The value 'QWER' is not valid."],"$.id":["'Q' is an invalid start of a value. Path: $.id | LineNumber: 1 | BytePositionInLine: 8."]}}
```

#### username=пусто - Users /api/v1/Users/{id}
**URL** https://fakerestapi.azurewebsites.net/api/v1/Users/{{activity_id}}     

**Ожидаемый результат** внести изменения записи 5 в username пусто ошибка


**Заголовки запроса**
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 55a48f63-fe5d-40ea-ade8-d937d6d4cafb
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
{
  "id": 5,
  "userName": ,
  "password": "string"
}
```

**Заголовки ответа** 
Content-Type: application/problem+json; charset=utf-8
Date: Sun, 11 Jun 2023 08:00:08 GMT
Server: Kestrel
Transfer-Encoding: chunked

**Тело ответа** 

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-f5f101b59f49fe4db84e84b6c99c5683-71597d210e7f0247-00","errors":{"$.userName":["',' is an invalid start of a value. Path: $.userName | LineNumber: 2 | BytePositionInLine: 14."]}}
```

#### id=-1 - /api/v1/Users/{id}
**URL** https://fakerestapi.azurewebsites.net/api/v1/Users/{{minis_id}}     

**Ожидаемый результат** внести изменения в запись с id -1 ошибка


**Заголовки запроса**
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 64589d2b-c403-4915-8b1b-fa136583d370
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
{
  "id": -1,
  "userName": "string",
  "password": "string"
}
```

**Заголовки ответа** 
Content-Type: application/json; charset=utf-8; v=1.0
Date: Sun, 11 Jun 2023 08:01:16 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0

**Тело ответа** 

```
{"id":-1,"userName":"string","password":"string"}
```

#### +Users: POST/Request body/5
**URL** https://fakerestapi.azurewebsites.net/api/v1/Users/     

**Ожидаемый результат** добавить запись 5


**Заголовки запроса**
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 4c2c99fd-5eef-4cc0-9097-28425b790972
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
{
  "id": 5,
  "userName": "string",
  "password": "string"
}
```

**Заголовки ответа** 
Content-Type: application/json; charset=utf-8; v=1.0
Date: Sun, 11 Jun 2023 08:02:21 GMT
Server: Kestrel
Transfer-Encoding: chunked
api-supported-versions: 1.0

**Тело ответа** 

```
{"id":5,"userName":"string","password":"string"}
```

#### id пусто -Users: POST/Request body/{id пусто}
**URL** https://fakerestapi.azurewebsites.net/api/v1/Users/     

**Ожидаемый результат** внести запись с пустым id ошибка


**Заголовки запроса**
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 6ba1cc9d-85c5-43ac-b400-f994243dce1c
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
{
  "id": ,
  "userName": "string",
  "password": "string"
}
```

**Заголовки ответа** 
Content-Type: application/problem+json; charset=utf-8
Date: Sun, 11 Jun 2023 08:03:29 GMT
Server: Kestrel
Transfer-Encoding: chunked

**Тело ответа** 

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-740181355bef744f8e54cd519598fefa-7921bf4b4c9cda46-00","errors":{"$.id":["',' is an invalid start of a value. Path: $.id | LineNumber: 1 | BytePositionInLine: 8."]}}
```

#### id буквы -Users: POST/Request body/{id QWERT}
**URL** https://fakerestapi.azurewebsites.net/api/v1/Users/     

**Ожидаемый результат** внести запись с id буквами ошибка


**Заголовки запроса**
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 71a63d39-df19-48d2-8452-530a6c2a1ace
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
{
  "id": QWER,
  "userName": "string",
  "password": "string"
}
```

**Заголовки ответа** 
Content-Type: application/problem+json; charset=utf-8
Date: Sun, 11 Jun 2023 08:04:38 GMT
Server: Kestrel
Transfer-Encoding: chunked

**Тело ответа** 

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-44b7bc820bd2de45ba8518b43722b20a-c5934be6cc86544a-00","errors":{"$.id":["'Q' is an invalid start of a value. Path: $.id | LineNumber: 1 | BytePositionInLine: 8."]}}
```

#### username=пусто - Users POST/Request body/
**URL** https://fakerestapi.azurewebsites.net/api/v1/Users/      

**Ожидаемый результат** добавить запись с пустым username ошибка 


**Заголовки запроса**
Content-Type: application/json
User-Agent: PostmanRuntime/7.32.2
Accept: */*
Cache-Control: no-cache
Postman-Token: 18539ee5-f59d-4629-8fa3-050c1ade47ff
Host: fakerestapi.azurewebsites.net
Accept-Encoding: gzip, deflate, br
Connection: keep-alive

**Тело запроса**

```
{
  "id": 5,
  "userName": ,
  "password": "string"
}
```

**Заголовки ответа** 
Content-Type: application/problem+json; charset=utf-8
Date: Sun, 11 Jun 2023 08:05:49 GMT
Server: Kestrel
Transfer-Encoding: chunked

**Тело ответа** 

```
{"type":"https://tools.ietf.org/html/rfc7231#section-6.5.1","title":"One or more validation errors occurred.","status":400,"traceId":"00-baa05fa9d473fe43aaa4ec3751c2a27a-2d200e25e24cb541-00","errors":{"$.userName":["',' is an invalid start of a value. Path: $.userName | LineNumber: 2 | BytePositionInLine: 14."]}}
```



